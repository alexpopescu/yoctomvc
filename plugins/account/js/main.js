$(function() {
    $.each($('#login-box,#login-form'), function () {
        var $form = $(this),
            id = $form.attr('id');
        $form.on('click', 'button[type="submit"]', function () {
            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serializeArray()
            }).done(function (data) {
                $form.find('div.form-group').removeClass('has-error');
                if (data.ok) {
                    var redirect = $form.find('[name="redirect"]');
                    if (redirect.length > 0 && redirect.val())
                        window.location.replace(redirect.val());
                    else location.reload();
                } else {
                    $('#' + id + '-error').html(data.msg).show();
                    if (data.fields && data.fields.length)
                        $.each(data.fields, function (index, value) {
                            $('#' + id + '-' + value).closest('div.form-group').addClass('has-error');
                        });
                }
            });
            return false;
        });
        $('#' + id + '-error').hide();
    });

    $('#btn-account-logout').on('click', function () {
        $.ajax({
            type: "POST",
            url: $(this).attr('href')
        }).done(function (data) {
            if (data.error) {
                $.alert({
                    'title': 'Error',
                    'text': data.error
                });
            } else {
                location.reload();
            }
        });
        return false;
    });
});
