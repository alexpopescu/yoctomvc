<?php
namespace NeuralToys\YoctoMVC\Plugins\Account {
    use Exception;
    use NeuralToys\YoctoMVC\Core\DataException;

    /**
     * Used when the username to be created already exists
     */
    class UsernameExistsException extends DataException { }

    /**
     * Used when the user does not have the necessary rights for the current operation
     */
    class InsufficientRightsException extends Exception { }
}


