<?php

// add session method to check for a set of rights for the current user
use NeuralToys\YoctoMVC\Helpers\Session_Helper;

Session_Helper::add_method('is_signed_in', function($session) {
    return !!$session->get('account');
});

Session_Helper::add_method('has_rights', function($session) {
    if (!$session->is_signed_in($session)) return false;
    $rights = func_get_args();
    $user_rights = $session->get('account', 'rights');
    if (empty($user_rights)) return false;
    for ($i = 1; $i < count($rights); $i++)
        if (!in_array($rights[$i], $user_rights)) return false;
    return true;
});
