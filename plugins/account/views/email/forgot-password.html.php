Hello <?php echo $this->name; ?>,
<br/>
You have just requested a password reset. Below you can see your new account credentials.<br/>
It is recommended changing your password to something easier to remember.
<br />
<br />
Email: <b><?php echo $this->email; ?></b><br/>
New password: <b><?php echo $this->password; ?></b><br/>
<br />
<br />
This is an automated email. Please do not reply to this email message as the reply will not be read.