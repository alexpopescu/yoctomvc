Hello <?php echo $this->name; ?>,
You have just requested a password reset. Below you can see your new account credentials.
It is recommended changing your password to something easier to remember.

Email: <?php echo $this->email; ?>

New password: <?php echo $this->password; ?>

This is an automated email. Please do not reply to this email message as the reply will not be read.