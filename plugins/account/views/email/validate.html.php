Hello <?php echo $this->name; ?>,
<br/>
You have just changed the email address registered for your "<?php echo $this->application; ?>" account.
You need to validate your email address in order to re-activate your account.
Please visit this url:
<br/>
<?php echo $this->url; ?>
<br/>
or click <a href="<?php echo $this->url; ?>">here</a> to complete your registration.
<br/>
<br/>
This is an automated email. Please do not reply to this email message as the reply will not be read.
<br/>
<br/>
Best Regards,
The <?php echo $this->application; ?> team
