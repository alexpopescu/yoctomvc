Hello <?php echo $this->name; ?>,

You have just changed the email address registered for your "<?php echo $this->application; ?>" account.
You need to validate your email address in order to re-activate your account.
Please visit this url:

<?php echo $this->url; ?>

to complete your registration.

This is an automated email. Please do not reply to this email message as the reply will not be read.

Best Regards,
The <?php echo $this->application; ?> team
