Hello <?php echo $this->name; ?>,
<br/>
You have just created a new account for <?php echo $this->application; ?>.
You need to validate your email address in order to activate your account.
Please visit this url:
<br/>
<?php echo $this->url; ?>
<br/>
or click <a href="<?php echo $this->url; ?>">here</a> to complete your registration.
<br/>
<br/>
This is an automated email. Please do not reply to this email message as the reply will not be read.