<?php
namespace NeuralToys\YoctoMVC\Plugins\Account\Models {
    use NeuralToys\YoctoMVC\Core\DataException;
    use NeuralToys\YoctoMVC\Core\Model;
    use NeuralToys\YoctoMVC\Plugins\Account\UsernameExistsException;
    use NeuralToys\YoctoMVC\Tools\Date_Tool;
    use PDO;

    /**
     * The User Model does the back-end work for the User Controller
     */
    class User_Model extends Model {

        /**
         * Check if the credentials are valid
         * @param string $email
         * @param string $password
         * @return null
         */
        public function check($email, $password) {
            $enc = sha1($password . $this->config->get('account', 'password_salt'));
            $user = parent::single('SELECT u.`pk_user`, CONCAT(u.`first_name`, \' \', u.`last_name`) AS name, u.`email`, t.`name` AS `timezone`
              FROM `account_user` u
              LEFT JOIN `timezone` t ON u.`fk_timezone` = t.`pk_timezone`
              WHERE u.`email` = :email AND u.`status` = \'active\' AND u.`password` = :password', array(
                'email' => $email,
                'password' => $enc
            ));
            if (!empty($user))
                $user['rights'] = $this->get_rights($user['pk_user']);
            return $user;
        }

        /**
         * Check if an email already exists in the database
         * @param string $email
         * @param int $pk_user
         * @return bool|null
         */
        public function exists($email, $pk_user = 0) {
            $res = parent::single('SELECT COUNT(1) AS found FROM `account_user` u WHERE u.`email` = :email AND u.`pk_user` != :pk_user AND u.`status` != \'deleted\'',
                array('email' => $email, 'pk_user' => $pk_user));
            return !empty($res['found']) && $res['found'] > 0;
        }

        /**
         * Save an account's information
         * @param string $email
         * @param string $password
         * @param string $first_name
         * @param string $last_name
         * @param string $url
         * @param int $fk_timezone
         * @param string $status
         * @param int $pk_user
         * @return null
         * @throws UsernameExistsException
         */
        public function save($email, $password, $first_name, $last_name, $url, $fk_timezone, $status, $pk_user = 0) {
            if ($this->exists($email, $pk_user)) throw new UsernameExistsException('An account with email '.$email.' is already registered');

            if (!$fk_timezone) $fk_timezone = null;
            $params =  array(
                'email' => $email,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'url' => $url,
                'fk_timezone' => $fk_timezone,
                'status' => $status);

            if ($pk_user) {
                $params['pk_user'] = $pk_user;
                parent::execute('UPDATE `account_user` SET
                  `email` = COALESCE(:email, `email`),
                  `first_name` = :first_name,
                  `last_name` = :last_name,
                  `url` = :url,
                  `fk_timezone` = :fk_timezone,
                  `status` = COALESCE(:status, `status`),
                  `update_date` = UNIX_TIMESTAMP()
                  WHERE `pk_user` = :pk_user', $params);
                return $pk_user;
            }

            $params['password'] = sha1($password . $this->config->get('account', 'password_salt'));
            parent::execute('INSERT INTO `account_user`
              (`email`, `first_name`, `last_name`, `password`, `url`, `fk_timezone`, `status`, `create_date`, `update_date`)
              VALUES (:email, :first_name, :last_name, :password, :url, :fk_timezone, :status, UNIX_TIMESTAMP(), UNIX_TIMESTAMP())', $params);
            return $this->db->lastInsertId();
        }

        /**
         * Save the roles granted to an account
         * @param int $pk_user
         * @param int $current_user
         * @param array $roles
         * @return null
         */
        public function save_roles($pk_user, $current_user, $roles = array()) {
            parent::execute('DELETE FROM `account_user_role` WHERE `fk_user` = :pk_user', array( 'pk_user' => $pk_user ));
            if ($roles)
            foreach ($roles as $role) {
                parent::execute('INSERT INTO `account_user_role` (`fk_user`, `fk_role`, `granted_by`, `grant_date`) VALUES (:fk_user, :fk_role, :pk_user, NOW())', array(
                    'fk_user' => $pk_user,
                    'fk_role' => $role,
                    'pk_user' => $current_user
                ));
            }
        }

        /**
         * Save the rights granted to an account
         * @param int $pk_user
         * @param int $current_user
         * @param array $rights
         * @return null
         */
        public function save_rights($pk_user, $current_user, $rights = array()) {
            parent::execute('DELETE FROM `account_user_right` WHERE `fk_user` = :pk_user', array( 'pk_user' => $pk_user ));
            foreach ($rights as $right) {
                parent::execute('INSERT INTO `account_user_right` (`fk_user`, `fk_right`, `granted_by`, `grant_date`) VALUES (:fk_user, :fk_right, :pk_user, NOW())', array(
                    'fk_user' => $pk_user,
                    'fk_right' => $right,
                    'pk_user' => $current_user
                ));
            }
        }

        /**
         * Create a new validation key
         * @param string $email
         * @return null|string
         * @throws DataException
         */
        public function create_key($email) {
            $key = null;
            $stmt = parent::statement('SELECT COUNT(1) AS found FROM `account_user` au WHERE au.`key` = :key', array( 'key' => $key ));
            $found = true;
            while ($found) {
                $key = random_string(32);
                if (!$stmt->execute()) continue;
                $res = $stmt->fetch(PDO::FETCH_ASSOC);
                $found = !empty($res['found']) && $res['found'] > 0;
            }

            parent::execute('UPDATE `account_user` SET `status` = \'unconfirmed\', `key` = :key WHERE `email` = :email', array(
                'email' => $email,
                'key' => $key
            ));
            return $key;
        }

        /**
         * Activate an account
         * @param string $key
         * @return bool
         * @throws DataException
         */
        public function activate($key) {
            $res = parent::single('SELECT COUNT(1) AS found FROM `account_user` au WHERE `status` = \'unconfirmed\' AND au.`key` = :key', array('key' => $key));
            if (!$res || empty($res['found']) || !$res['found'])
                throw new DataException('Invalid activation; The account does not exist or does not need activation');

            parent::execute('UPDATE `account_user` SET `status` = \'active\', `key` = NULL WHERE `key` = :key', array('key' => $key));
            return true;
        }

        /**
         * Change the password of the current account
         * @param int $pk_user
         * @param string $password
         * @return null|string
         */
        public function change_password($pk_user, $password) {
            parent::execute('UPDATE `account_user` SET `password` = :password WHERE `pk_user` = :pk_user', array(
                'password' => sha1($password . $this->config->get('account', 'password_salt')),
                'pk_user' => $pk_user
            ));
        }

        /**
         * Reset the password of an account
         * @param string $email
         * @return null|string
         */
        public function reset_password($email) {
            $password = strtolower(random_string(8));
            $encoded = sha1($password . $this->config->get('account', 'password_salt'));
            if (parent::execute('UPDATE `account_user` SET `password` = :password WHERE `email` = :email', array(
                'email' => $email,
                'password' => $encoded
            ))) return $password;
            return null;
        }

        /**
         * Get the information of an account by the email address
         * @param string $email
         * @return mixed|null
         */
        public function get_by_email($email) {
            return parent::single('SELECT `pk_user`, CONCAT(`first_name`, \' \', `last_name`) AS full_name
                FROM `account_user` WHERE `email` = :email', array( 'email' => $email ));
        }

        /**
         * Get the information of an account by primary key
         * @param int $pk_user
         * @param string|null $timezone
         * @param bool $include_rights
         * @return mixed|null
         */
        public function get($pk_user, $timezone, $include_rights = true) {
            $ret = parent::single('SELECT u.`pk_user`, u.`email`, u.`first_name`, u.`last_name`, u.`url`, u.`status`,
                u.`create_date`, u.`update_date`, u.`fk_timezone`, t.`name` AS `timezone`
              FROM `account_user` u
              LEFT JOIN `timezone` t ON u.`fk_timezone` = t.`pk_timezone`
              WHERE u.`pk_user` = :pk_user', array( 'pk_user' => $pk_user ));
            if (!$ret) return null;

            if ($include_rights) {
                $roles = parent::query('
                    SELECT r.`pk_role`, r.`name`
                    FROM `account_user_role` ur
                     LEFT JOIN `account_role` r ON ur.`fk_role` = r.`pk_role`
                    WHERE `fk_user` = :fk_user', array('fk_user' => $pk_user));
                $ret['roles'] = $roles;

                $rights = parent::query('
                    SELECT r.`pk_right`, r.`name`
                    FROM `account_user_right` ur
                      LEFT JOIN `account_right` r ON ur.`fk_right` = r.`pk_right`
                    WHERE `fk_user` = :fk_user', array('fk_user' => $pk_user));
                $ret['rights'] = $rights;
            }

            $ret['create_date'] = Date_Tool::timestamp_to_date($ret['create_date'], $timezone);
            $ret['update_date'] = Date_Tool::timestamp_to_date($ret['update_date'], $timezone);
            return $ret;
        }

        /**
         * Get the rights associated with an account
         * @param $pk_user
         * @return array
         */
        public function get_rights($pk_user) {
            $res = parent::query('SELECT ar.`name`
              FROM `account_user_right` aur
                LEFT JOIN `account_right` ar ON aur.`fk_right` = ar.`pk_right`
              WHERE aur.`fk_user` = :pk_user
              UNION DISTINCT
              SELECT ar.`name`
              FROM `account_role_right` arr
                LEFT JOIN `account_right` ar ON arr.`fk_right` = ar.`pk_right`
                LEFT JOIN `account_user_role` aur ON arr.`fk_role` = aur.`fk_role`
              WHERE aur.`fk_user` = :pk_user', array( 'pk_user' => $pk_user ));
            if (!$res) return array();
            return array_map(function($x) { return $x['name']; }, $res);
        }

        /**
         * Get the list of accounts
         * @param string $timezone
         * @return array
         */
        public function listing($timezone) {
            $res = parent::query('
              SELECT u.`pk_user`, u.`email`, u.`first_name`, u.`last_name`, u.`url`, u.`status`, u.`create_date`, u.`update_date`, t.`name` AS `timezone`
              FROM `account_user` u
                LEFT JOIN `timezone` t ON u.`fk_timezone` = t.`pk_timezone`');

            for ($i = 0; $i < sizeof($res); $i++) {
                $res[$i]['create_date'] = array(
                    'timestamp' => intval($res[$i]['create_date']),
                    'display' => Date_Tool::timestamp_to_date($res[$i]['create_date'], $timezone)->format('d/m/Y H:i:s')
                );
                $res[$i]['update_date'] = array(
                    'timestamp' => intval($res[$i]['update_date']),
                    'display' => Date_Tool::timestamp_to_date($res[$i]['update_date'], $timezone)->format('d/m/Y H:i:s')
                );
            }
            return $res;
        }

        /**
         * Get the list of possible user statuses
         * @return array
         */
        public function get_all_statuses() {
            $res = parent::single('SHOW COLUMNS FROM `account_user` WHERE Field = \'status\'');
            preg_match('/^enum\(\'(.*)\'\)$/', $res['Type'], $matches);
            $enum = explode('\',\'', $matches[1]);
            return $enum;
        }

        /**
         * Get the default user status
         * @return array
         */
        public function get_default_status() {
            $res = parent::single('SHOW COLUMNS FROM `account_user` WHERE Field = \'status\'');
            return $res['Default'];
        }

        /**
         * Change the status of an account
         * @param $pk_user
         * @param $status
         * @return mixed
         */
        public function status($pk_user, $status) {
            return parent::update('account_user', array('status' => $status), array('pk_user' => $pk_user));
        }

        /**
         * Hard delete an account
         * @param $pk_user
         * @return mixed
         */
        public function del($pk_user) {
            return parent::delete('account_user', array('pk_user' => $pk_user));
        }
    }
}