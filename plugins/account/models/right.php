<?php
namespace NeuralToys\YoctoMVC\Plugins\Account\Models {
    use NeuralToys\YoctoMVC\Core\Model;
    use PDO;

    /**
     * The Right Model does the data work for operations related to rights
     */
    class Right_Model extends Model {

        /**
         * Get the list of rights
         * @return array
         */
        public function listing() {
            return parent::query('SELECT `pk_right`, `name` FROM `account_right`');
        }

        /**
         * Get the information of a right by primary key
         * @param int $pk_right
         * @return mixed|null
         */
        public function get($pk_right) {
            return parent::single('SELECT `pk_right`, `name` FROM `account_right` WHERE `pk_right` = :pk_right', array(
                'pk_right' => $pk_right
            ));
        }

        /**
         * Save a right information
         * @param int $pk_right
         * @param string $name
         * @return null
         */
        public function save($pk_right, $name) {
            if ($pk_right) {
                parent::update('account_right', array( 'name' => $name ), array( 'pk_right' => $pk_right ));
                return $pk_right;
            }
            return intval(parent::insert('account_right', array( 'name' => $name )));
        }

        /**
         * Delete an existing right
         * @param int $pk_right
         * @return mixed|null
         */
        public function del($pk_right) {
            return parent::delete('account_right', array(
                'pk_right' => $pk_right
            ));
        }
    }
}