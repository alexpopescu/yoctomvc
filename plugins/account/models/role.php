<?php
namespace NeuralToys\YoctoMVC\Plugins\Account\Models {
    use NeuralToys\YoctoMVC\Core\Model;
    use PDO;

    /**
     * The Role Model does the data work for operations related to roles
     */
    class Role_Model extends Model {

        /**
         * Get the list of roles
         * @return array
         */
        public function listing() {
            $res = parent::query('SELECT ro.`pk_role`, ro.`name`, ri.`name` AS `right`
              FROM `account_role_right` rr
                LEFT JOIN `account_role` ro ON rr.`fk_role` = ro.`pk_role`
                LEFT JOIN `account_right` ri ON rr.`fk_right` = ri.`pk_right`
              ORDER BY `pk_role` ASC, `pk_right` ASC');
            $ret = array();
            foreach ($res as $item) {
                if (!isset($ret[$item['pk_role']]))
                    $ret[$item['pk_role']] = array(
                        'pk_role' => $item['pk_role'],
                        'name' => $item['name'],
                        'rights' => array()
                    );
                $ret[$item['pk_role']]['rights'][] = $item['right'];
            }
            return array_values($ret);
        }

        /**
         * Get the information of a role by primary key
         * @param int $pk_role
         * @return mixed|null
         */
        public function get($pk_role) {
            $ret = parent::single('SELECT `pk_role`, `name` FROM `account_role` WHERE `pk_role` = :pk_role', array(
                'pk_role' => $pk_role
            ));

            $rights = parent::query('
                SELECT r.`pk_right`, r.`name`
                FROM `account_role_right` rr
                  LEFT JOIN `account_right` r ON rr.`fk_right` = r.`pk_right`
                WHERE rr.`fk_role` = :fk_role', array( 'fk_role' => $pk_role ));
            $ret['rights'] = $rights;

            return $ret;
        }

        /**
         * Save a role information
         * @param int $pk_role
         * @param string $name
         * @return int
         */
        public function save($pk_role, $name) {
            if ($pk_role) {
                parent::update('account_role', array( 'name' => $name ), array( 'pk_role' => $pk_role ));
                return $pk_role;
            }
            return intval(parent::insert('account_role', array( 'name' => $name )));
        }

        /**
         * Save the rights included in a role
         * @param int $pk_role
         * @param array $rights
         * @return null
         */
        public function save_rights($pk_role, $rights = array()) {
            parent::execute('DELETE FROM `account_role_right` WHERE `fk_role` = :pk_role', array( 'pk_role' => $pk_role ));
            foreach ($rights as $right) {
                parent::execute('INSERT INTO `account_role_right` (`fk_role`, `fk_right`) VALUES (:fk_role, :fk_right)', array(
                    'fk_role' => $pk_role,
                    'fk_right' => $right
                ));
            }
        }

        /**
         * Delete an existing role
         * @param int $pk_role
         * @return mixed|null
         */
        public function del($pk_role) {
            return parent::delete('account_role', array(
                'pk_role' => $pk_role
            ));
        }

        /**
         * Get the rights associated with a role
         * @param $pk_role
         * @return array
         */
        public function get_rights($pk_role) {
            $db = parent::connect();
            $stmt = $db->prepare('CALL `account_user_get_rights`(:pk_user)');
            $stmt->bindParam(':pk_user', $pk_user, PDO::PARAM_INT);
            $ret = array();
            if ($stmt->execute()) {
                $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach ($res as $r) $ret[] = $r['name'];
            }
            return $ret;
        }
    }
}