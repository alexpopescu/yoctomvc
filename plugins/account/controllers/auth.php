<?php
namespace NeuralToys\YoctoMVC\Plugins\Account\Controllers {
    use Exception;
    use NeuralToys\YoctoMVC\Core\Parameters;
    use NeuralToys\YoctoMVC\Plugins\Account\Models\Right_Model;
    use NeuralToys\YoctoMVC\Plugins\Account\Models\Role_Model;

    /**
     * This file handles the management of users
     */
    class Auth_Controller extends Account_Controller
    {
        /**
         * The roles listing
         */
        public function roles()
        {
            $this->validate_rights('manage_rights_roles', '%');

            $this->view(array(
                'title' => $this->language->get('roles_title', 'account')
            ));
        }

        /**
         * Get the list of roles
         */
        public function get_all_roles()
        {
            $this->validate_rights('manage_rights_roles', 403);

            try {
                $model = new Role_Model($this->config);
                $this->json($model->listing());
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * Get a role by id
         * @param Parameters $params
         */
        public function get_role(Parameters $params)
        {
            $this->validate_rights('manage_rights_roles', 403);

            try {
                $model = new Role_Model($this->config);
                $this->json($model->get($params->id));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * Save a new or existing role
         * @param Parameters $params
         */
        public function save_role(Parameters $params)
        {
            $this->validate_method('post');
            $this->validate_rights('manage_rights_roles', 403);

            try {
                $model = new Role_Model($this->config);
                $pk_role = $params->pk_role;
                if (!$pk_role) $pk_role = 0;
                $pk_role = $model->save($pk_role, $params->name);

                if ($params->rights) $model->save_rights($pk_role, explode(',', $params->rights));
                else $model->save_rights($pk_role);

                $this->json_success($this->language->get('msg_role_saved', 'account'));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * Delete an existing role
         * @param Parameters $params
         */
        public function delete_role(Parameters $params)
        {
            $this->validate_rights('manage_rights_roles', 403);

            try {
                $model = new Role_Model($this->config);
                $model->del($params->id);
                $this->json_success($this->language->get('msg_role_deleted', 'account'));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * The rights listing
         */
        public function rights()
        {
            $this->validate_rights('manage_rights_roles', '%');

            $this->view(array(
                'title' => $this->language->get('rights_title', 'account')
            ));
        }

        /**
         * Get the list of rights
         */
        public function get_all_rights()
        {
            $this->validate_rights('manage_rights_roles', 403);

            try {
                $model = new Right_Model($this->config);
                $this->json($model->listing());
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * Get a right by id
         * @param Parameters $params
         */
        public function get_right(Parameters $params)
        {
            $this->validate_rights('manage_rights_roles', 403);

            try {
                $model = new Right_Model($this->config);
                $this->json($model->get($params->id));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * Save a new or existing right
         * @param Parameters $params
         */
        public function save_right(Parameters $params)
        {
            $this->validate_method('post');
            $this->validate_rights('manage_rights_roles', 403);

            try {
                $model = new Right_Model($this->config);
                $pk_right = $params->pk_right;
                if (!$pk_right) $pk_right = 0;
                $model->save($pk_right, $params->name);
                $this->json_success($this->language->get('msg_right_saved', 'account'));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * Delete an existing right
         * @param Parameters $params
         */
        public function delete_right(Parameters $params)
        {
            $this->validate_rights('manage_rights_roles', 403);

            try {
                $model = new Right_Model($this->config);
                $model->del($params->id);
                $this->json_success($this->language->get('msg_right_deleted', 'account'));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }
    }
}
