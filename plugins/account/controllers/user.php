<?php
namespace NeuralToys\YoctoMVC\Plugins\Account\Controllers {
    use Exception;
    use Externals;
    use NeuralToys\YoctoMVC\Core\Parameters;
    use NeuralToys\YoctoMVC\Core\View_Handler;
    use NeuralToys\YoctoMVC\Plugins\Account\Models\User_Model;

    /**
     * This file handles the management of users
     */
    class User_Controller extends Account_Controller
    {
        /**
         * The user listing page
         */
        public function listing()
        {
            $this->validate_rights('manage_users', '%');

            $this->view(array(
                'title' => $this->language->get('users_title', 'account')
            ));
        }

        public function get_all()
        {
            $this->validate_rights('manage_users', 403);

            try {
                $model = new User_Model($this->config);
                $this->json($model->listing($this->session->get('account', 'timezone')));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        public function get_all_statuses()
        {
            $this->validate_rights('manage_users', 403);

            try {
                $model = new User_Model($this->config);
                $this->json($model->get_all_statuses());
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        public function get(Parameters $params)
        {
            if (!$params->id) $this->validate_session(403);

            try {
                $model = new User_Model($this->config);
                $this->json($model->get($params->id ?: $this->session->get('account', 'pk_user'), $this->session->get('account', 'timezone')));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        public function save(Parameters $params)
        {
            $this->validate_method('post');
            $this->validate_rights('manage_users', 403);

            if ($params->validate_exists('email', $this->language->get('msg_enter_email', 'account'), 'user-profile-email'))
                $params->validate_email('email', $this->language->get('msg_email_invalid', 'account'), 'user-profile-email');

            if ($params->has_errors()) {
                $this->json($params->get_errors());
                return;
            }

            try {
                $model = new User_Model($this->config);
                $pk_user = $model->save($params->email, null, $params->first_name, $params->last_name, $params->url, $params->fk_timezone, $params->status, $params->pk_user);

                if ($params->roles) $model->save_roles($pk_user, $this->session->get('account', 'pk_user'), explode(',', $params->roles));
                else $model->save_roles($pk_user, $this->session->get('account', 'pk_user'));
                if ($params->rights) $model->save_rights($pk_user, $this->session->get('account', 'pk_user'), explode(',', $params->rights));
                else $model->save_rights($pk_user, $this->session->get('account', 'pk_user'));

                $this->json_success($this->language->get('msg_account_saved', 'account'));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        public function delete(Parameters $params)
        {
            $this->validate_rights('delete_users', 403);

            try {
                $model = new User_Model($this->config);

                if ($this->config->get('account', 'hard_delete_users')) {
                    $model->del($params->id);
                    $this->json_success($this->language->get('msg_account_deleted', 'account'));
                    return;
                }

                $model->status($params->id, 'deleted');
                $this->json_success($this->language->get('msg_account_marked_deleted', 'account'));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * View a user profile
         */
        public function profile()
        {
            $this->validate_session('%');

            $this->view(array(
                'title' => $this->language->get('my_profile', 'account')
            ));
        }

        /**
         * Save user account
         * @param Parameters $params
         */
        public function save_profile(Parameters $params)
        {
            $this->validate_method('post');
            $this->validate_session(403);

            // validate email
            if ($params->validate_exists('email', $this->language->get('msg_enter_email', 'account'), 'user-profile-email'))
                $params->validate_email('email', $this->language->get('msg_email_invalid', 'account'), 'user-profile-email');
            // validate password
            $params->validate_equals_other('confirm', 'password', $this->language->get('msg_passwords_do_not_match', 'account'), 'user-profile-confirm');

            if ($params->has_errors()) {
                $this->json($params->get_errors());
                return;
            }

            try {
                $pk_user = $this->session->get('account', 'pk_user');
                $model = new User_Model($this->config);

                $email = $params->email;
                if (!$this->config->get('account', 'allow_changing_email')) {
                    $current = $model->get($pk_user, null, false);
                    $email = $current['email'];
                }
                if ($model->save($email, null, $params->first_name, $params->last_name, $params->url, $params->fk_timezone, null, $pk_user)) {
                    $msg = array();
                    $ok = true;

                    // check if we need to change the password
                    if ($params->password && $params->password == $params->confirm) {
                        $this->logger->info('Changing password for user '.$pk_user);
                        $model->change_password($pk_user, $params->password);
                        $msg[] = $this->language->get('msg_password_changed', 'account');
                    }
                    $msg[] = $this->language->get('msg_profile_saved', 'account');

                    // check if we need to revalidate the account
                    if ($email != $this->session->get('account', 'email')) {
                        $this->logger->info('Sending revalidation email for user '.$pk_user);
                        $key = $model->create_key($email);
                        Externals::load('phpmailer');
                        $mail = \Externals::getPhpMailer($this->config);
                        $full_name = trim($params->firstname . ' ' . $params->lastname);
                        if ($full_name) $mail->addAddress($email, $full_name);
                        else $mail->addAddress($email);

                        $mail->isHTML(true);
                        $mail->Subject = $this->language->get('validate_email', 'account');

                        $variables = array(
                            'name' => empty($full_name) ? $params->username : $full_name,
                            'application' => $this->config->get('core', 'application'),
                            'url' => $this->url->action('flow', 'activate', array('id' => $key), 'account', true)
                        );
                        $html = new View_Handler($this->config, $this->language, 'email' . DS . 'validate.html', false, 'account');
                        $html->assign($variables);
                        $mail->Body = $html->render(false);

                        $text = new View_Handler($this->config, $this->language, 'email' . DS . 'validate.text', false, 'account');
                        $text->assign($variables);
                        $mail->AltBody = $text->render(false);

                        if (!$mail->send()) {
                            $this->logger->error('Could not send revalidation email for user ' . $pk_user);
                            $msg[] = $this->language->get('msg_revalidate_failed', 'account');
                            $ok = false;
                        } else {
                            $this->logger->info('Revalidation email sent for user ' . $pk_user);
                            $msg[] = $this->language->get('msg_revalidate_successful', 'account');
                        }
                    }

                    // update the session information
                    $user_info = $model->get($pk_user, null, false);
                    $this->session->set('account', array(
                        'pk_user' => $user_info['pk_user'],
                        'name' => $user_info['first_name'] . ' ' . $user_info['last_name'],
                        'email' => $user_info['email'],
                        'timezone' => $user_info['timezone'],
                        'rights' => $model->get_rights($pk_user)
                    ));

                    $this->json(array(
                        'ok' => $ok,
                        'msg' => implode('<br/>', $msg)
                    ));
                } else {
                    $this->json_error($this->language->get('msg_could_not_save_profile', 'account'));
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }
    }
}
