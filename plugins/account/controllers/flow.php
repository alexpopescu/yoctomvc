<?php
namespace NeuralToys\YoctoMVC\Plugins\Account\Controllers {
    use Exception;
    use Externals;
    use InvalidArgumentException;
    use NeuralToys\YoctoMVC\Core\Controller;
    use NeuralToys\YoctoMVC\Core\DataException;
    use NeuralToys\YoctoMVC\Core\Parameters;
    use NeuralToys\YoctoMVC\Core\View_Handler;
    use NeuralToys\YoctoMVC\Plugins\Account\Models\User_Model;
    use NeuralToys\YoctoMVC\Plugins\Account\UsernameExistsException;

    /**
     * This file handles the account flow (login/logout/register/forgot pass/activate
     */
    class Flow_Controller extends Controller
    {
        /**
         * Display the login screen
         * @param Parameters $params
         */
        public function login(Parameters $params)
        {
            if ($this->session->get('account', 'pk_user')) $this->redirect($this->config->get('account', 'default_redirect_after_login'));

            $requested = $params->requested;
            if (empty($requested)) $requested = $this->config->get('account', 'default_redirect_after_login');

            $this->view(array(
                    'title' => $this->language->get('sign_in', 'account'),
                    'requested' => $requested
                )
            );
        }

        /**
         * Login / start new user session
         * @param Parameters $params
         */
        public function do_login(Parameters $params)
        {
            $this->validate_method('post');
            $params->validate_exists('email', $this->language->get('msg_enter_email', 'account'));
            $params->validate_exists('password', $this->language->get('msg_enter_password', 'account'));

            if ($params->has_errors()) {
                $this->json($params->get_errors());
                return;
            }

            try {
                $model = new User_Model($this->config);
                $ret = $model->check($params->email, $params->password);
                if (empty($ret)) {
                    $this->json_error($this->language->get('msg_login_failed', 'account'));
                    return;
                }

                $this->logger->debug('Signing in user ' . $ret['pk_user']);

                $this->session->set('account', $ret);
                $this->json(array('ok' => true));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * Logout / end user session
         */
        public function logout()
        {
            $this->validate_method('post');
            try {
                $this->logger->debug('Signing out user ' . $this->session->get('account', 'pk_user'));

                $this->session->clear();
                $this->json(array('ok' => true));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * Display the forgot password / user screen
         */
        public function forgot()
        {
            $this->view(array(
                'title' => $this->language->get('forgot_password', 'account')
            ));
        }

        /**
         * Reset password and send to user
         * @param Parameters $params
         * @throws DataException
         */
        public function do_forgot_password(Parameters $params)
        {
            $this->validate_method('post');
            $params->validate_exists('email', $this->language->get('msg_enter_email', 'account'), 'forgot-password-email');
            if ($params->has_errors()) {
                $this->json($params->get_errors());
                return;
            }
            try {
                $email_address = $params->email;
                $model = new User_Model($this->config);
                $user = $model->get_by_email($email_address);
                if ($user == null) {
                    $this->json_error($this->language->get('msg_email_not_found', 'account'));
                    return;
                }
                $full_name = $user['full_name'];
                $password = $model->reset_password($email_address);

                // send email message
                $this->logger->info('Sending forgot password email for user ' . $user['pk_user']);

                Externals::load('phpmailer');
                $mail = \Externals::getPhpMailer($this->config);
                if ($full_name) $mail->addAddress($email_address, $full_name);
                else $mail->addAddress($email_address);

                $mail->isHTML(true);
                $mail->Subject = $this->language->get('password_reset', 'account');

                $variables = array(
                    'email' => $params->email,
                    'password' => $password,
                    'name' => $full_name
                );
                $html = new View_Handler($this->config, $this->language, 'email' . DS . 'forgot-password.html', false, 'account');
                $html->assign($variables);
                $mail->Body = $html->render(false);

                $text = new View_Handler($this->config, $this->language, 'email' . DS . 'forgot-password.text', false, 'account');
                $text->assign($variables);
                $mail->AltBody = $text->render(false);

                if (!$mail->send()) {
                    $this->logger->error('Could not send forgot password email for user ' . $user['pk_user']);
                    $this->json_error($this->language->get('msg_forgot_password_failed', 'account'));
                } else {
                    $this->logger->info('Forgot password email sent for user ' . $user['pk_user']);
                    $this->json_success($this->language->get('msg_forgot_password_reset', 'account')
                        . '<br />' . $this->language->get('msg_forgot_password_advice', 'account'));
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * Display the register screen
         */
        public function register()
        {
            if (!$this->config->get('account', 'allow_registration')) $this->redirect(404, true);
            $this->view(array(
                'title' => $this->language->get('register', 'account')
            ));
        }

        /**
         * Create a new account
         * @param Parameters $params
         */
        public function do_register(Parameters $params)
        {
            if (!$this->config->get('account', 'allow_registration')) $this->redirect(404, true);
            $this->validate_method('post');
            // validate email
            if ($params->validate_exists('email', $this->language->get('msg_enter_email', 'account'), 'register-email'))
                $params->validate_email('email', $this->language->get('msg_email_invalid', 'account'), 'register-email');
            // validate password
            if ($params->validate_exists('password', $this->language->get('msg_enter_password', 'account'), 'register-password')) {
                $params->validate_regex('password', '/^[a-z0-9]{8,30}$/i', $this->language->get('msg_password_invalid', 'account'), 'register-password');
                $params->validate_equals_other('confirm', 'password', $this->language->get('msg_passwords_do_not_match', 'account'), 'register-confirm');
            }
            if ($params->has_errors()) {
                $this->json($params->get_errors());
                return;
            }

            $pk_user = null;
            $key = null;
            try {
                $model = new User_Model($this->config);
                try {
                    $status = $model->get_default_status();
                    $pk_user = $model->save($params->email, $params->password, $params->firstname, $params->lastname, $params->url, $params->timezone, $status);
                    $key = $model->create_key($params->email);
                } catch (UsernameExistsException $e) {
                    $params->add_error($this->language->get('msg_email_already_exists', 'account'), 'email', 'register-email');
                    $this->json($params->get_errors());
                    return;
                } catch (Exception $e) {
                    $this->json_error($this->language->get('msg_error'));
                    return;
                }
                if (empty($pk_user) || $pk_user == 0) throw new Exception('The creation of the user failed for unknown reasons');

                $full_name = trim($params->firstname . ' ' . $params->lastname);
                if (empty($full_name)) $full_name = null;

                // send registration email
                $this->logger->info('Sending registration email for user ' . $pk_user);
                Externals::load('phpmailer');
                $mail = \Externals::getPhpMailer($this->config);
                if ($full_name) $mail->addAddress($params->email, $full_name);
                else $mail->addAddress($params->email);

                $mail->isHTML(true);
                $mail->Subject = $this->language->get('activate_account', 'account');

                $variables = array(
                    'name' => empty($full_name) ? $params->username : $full_name,
                    'application' => $this->config->get('core', 'application'),
                    'url' => $this->url->action('flow', 'activate', array('id' => $key), 'account', true)
                );
                $html = new View_Handler($this->config, $this->language, 'email' . DS . 'register.html', false, 'account');
                $html->assign($variables);
                $mail->Body = $html->render(false);

                $text = new View_Handler($this->config, $this->language, 'email' . DS . 'register.text', false, 'account');
                $text->assign($variables);
                $mail->AltBody = $text->render(false);

                if (!$mail->send()) {
                    $this->logger->error('Could not send registration email for user ' . $pk_user);
                    $this->json_error($this->language->get('msg_register_failed', 'account'));
                } else {
                    $this->logger->info('Registration email sent for user ' . $pk_user);
                    $this->json_success($this->language->get('msg_register_successful', 'account')
                        . '<br />' . $this->language->get('msg_register_validate', 'account'));
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        /**
         * Activates an inactive user (clicked the link that comes in email address)
         * @param Parameters $params
         * @throws InvalidArgumentException
         */
        public function activate(Parameters $params)
        {
            try {
                if (!$params->validate_exists('id', null, null)) {
                    throw new InvalidArgumentException($this->language->get('msg_invalid_key', 'account'));
                }
                $model = new User_Model($this->config);
                $model->activate($params->id);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->view(array(
                    'ok' => false,
                    'error' => $this->language->get('msg_error')
                ));
                return;
            }
            $this->view(array('ok' => true));
        }

    }
}
