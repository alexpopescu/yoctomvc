<?php
namespace NeuralToys\YoctoMVC\Plugins\Account\Controllers {
    use NeuralToys\YoctoMVC\Core\Controller;

    /**
     * This is the base controller, that should be inherited by all controllers
     */
    abstract class Account_Controller extends Controller {
        /**
         * Checks if the used HTTP method is acceptable for this action
         * The parameters are a list of rights and a url
         * If the url is a number it will be considered the code of the response (e.g. 404 = Not Found)
         * @param null $url The url to redirect to. If it's a number then it will become the code of the response.
         * If not specified, the browser will be redirected to the login page
         * @return bool
         */
        protected final function validate_session($url = null) {
            if ($this->session->is_signed_in()) return true;
            if (!$url || $url == '%') $url = $this->get_login_url();
            $this->redirect($url, true);
        }

        /**
         * Checks if the used HTTP method is acceptable for this action
         * The parameters are a list of rights and a url
         * If the url is a number it will be considered the code of the response (e.g. 404 = Not Found)
         */
        protected final function validate_rights() {
            $args = func_get_args();
            $rights = array_slice($args, 0, sizeof($args) - 1);
            $url = $args[sizeof($args) - 1];
            if ($url == '%') $url = $this->get_login_url();

            if (call_user_func_array(array($this->session, 'has_rights'), $rights)) return true;
            $this->redirect($url ?: $this->get_login_url(), true);
        }

        private function get_login_url() {
            return $this->url->action('flow', 'login', array(
                'requested' => $this->url->current()
            ), 'account');
        }
    }
}