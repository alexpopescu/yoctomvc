
<table id="roles" class="table table-striped table-bordered" cellspacing="0" width="100%"
       data-editor="#role-edit" data-success="[data-alert=&quot;success&quot;]" data-error="[data-alert=&quot;error&quot;]">
    <thead>
        <tr>
            <th><span class="glyphicon glyphicon-plus text-success"
                      title="<?php echo $this->language->get('add_role_title', 'account'); ?>"></span></th>
            <th><?php echo $this->language->get('name', 'account'); ?></th>
            <th><?php echo $this->language->get('rights', 'account'); ?></th>
        </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
        <tr>
            <th></th>
            <th><?php echo $this->language->get('name', 'account'); ?></th>
            <th><?php echo $this->language->get('rights', 'account'); ?></th>
        </tr>
    </tfoot>
</table>


<!-- editor form -->
<div id="role-edit" class="modal fade" data-keyboard="true"
     data-get-url="<?php echo $this->url->action('auth', 'get_role', null, 'account') ?>"
     data-add-title="<?php echo $this->language->get('add_role_title', 'account'); ?>"
     data-edit-title="<?php echo $this->language->get('edit_role_title', 'account'); ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="role-edit-form" action="<?php echo $this->url->action('auth', 'save_role', null, 'account') ?>" method="post"
                      data-success="[data-alert=&quot;success&quot;]" data-error="[data-alert=&quot;form-error&quot;]">

                    <input type="hidden" id="role-edit-id" name="pk_role" />
                    <div data-alert="form-error" class="alert alert-danger" style="display: none;"></div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="role-edit-name"><?php echo $this->language->get('name', 'account'); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="role-edit-name" name="name" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="user-edit-rights"><?php echo $this->language->get('rights', 'account'); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="user-edit-rights" name="rights" class="form-control" data-role="tags"
                                   data-source="<?php echo $this->url->action('auth', 'get_all_rights', null, 'account') ?>"
                                   data-identity="pk_right" data-display="name" />
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->language->get('cancel'); ?></button>
                <button data-submit="#role-edit-form" type="button" class="btn btn-primary"><?php echo $this->language->get('save'); ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- actions template -->
<div id="actions" style="display: none;">
    <span class="glyphicon glyphicon-edit" title="<?php echo $this->language->get('edit_role_title', 'account'); ?>"></span>
    <span class="glyphicon glyphicon-remove text-danger"
          data-url = "<?php echo $this->url->action('auth', 'delete_role', null, 'account') ?>"
          data-confirm = "<?php echo $this->language->get('confirmation'); ?>"
          title="<?php echo $this->language->get('delete_role_title', 'account'); ?>"></span>
</div>


<!-- SectionStart: script -->
<script type="application/javascript">
    $(document).ready(function() {
        // initialize data table
        $('#roles').DataTable({
            deferRender: true,
            processing: true,
            ajax: {
                url: "<?php echo $this->url->action('auth', 'get_all_roles', null, 'account') ?>",
                dataSrc: ""
            },
            rowId: 'pk_role',
            columns: [
                {
                    data: null,
                    orderable: false,
                    defaultContent: $('#actions').html(),
                    className: 'centered'
                },
                { data: 'name' },
                { data: 'rights[, ]' }
            ],
            order: [[ 1, 'asc' ]]
        });
    });
</script>
<!-- SectionEnd -->
