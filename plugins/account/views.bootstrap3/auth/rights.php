
<table id="rights" class="table table-striped table-bordered" cellspacing="0" width="100%"
       data-editor="#right-edit" data-success="[data-alert=&quot;success&quot;]" data-error="[data-alert=&quot;error&quot;]">
    <thead>
        <tr>
            <th><span class="glyphicon glyphicon-plus text-success"
                      title="<?php echo $this->language->get('add_right_title', 'account'); ?>"></span></th>
            <th><?php echo $this->language->get('name', 'account'); ?></th>
        </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
        <tr>
            <th></th>
            <th><?php echo $this->language->get('name', 'account'); ?></th>
        </tr>
    </tfoot>
</table>


<!-- editor form -->
<div id="right-edit" class="modal fade" data-keyboard="true"
     data-get-url="<?php echo $this->url->action('auth', 'get_right', null, 'account') ?>"
     data-add-title="<?php echo $this->language->get('add_right_title', 'account'); ?>"
     data-edit-title="<?php echo $this->language->get('edit_right_title', 'account'); ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="right-edit-form" action="<?php echo $this->url->action('auth', 'save_right', null, 'account') ?>" method="post"
                      data-success="[data-alert=&quot;success&quot;]" data-error="[data-alert=&quot;form-error&quot;]">

                    <input type="hidden" id="right-edit-id" name="pk_right" />
                    <div data-alert="form-error" class="alert alert-danger" style="display: none;"></div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="right-edit-name"><?php echo $this->language->get('name', 'account'); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="right-edit-name" name="name" class="form-control" />
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->language->get('cancel'); ?></button>
                <button data-submit="#right-edit-form" type="button" class="btn btn-primary"><?php echo $this->language->get('save'); ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- actions template -->
<div id="actions" style="display: none;">
    <span class="glyphicon glyphicon-edit" title="<?php echo $this->language->get('edit_right_title', 'account'); ?>"></span>
    <span class="glyphicon glyphicon-remove text-danger"
          data-url = "<?php echo $this->url->action('auth', 'delete_right', null, 'account') ?>"
          data-confirm = "<?php echo $this->language->get('confirmation'); ?>"
          title="<?php echo $this->language->get('delete_right_title', 'account'); ?>"></span>
</div>


<!-- SectionStart: script -->
<script type="application/javascript">
    $(document).ready(function() {
        // initialize data table
        $('#rights').DataTable({
            deferRender: true,
            processing: true,
            ajax: {
                url: "<?php echo $this->url->action('auth', 'get_all_rights', null, 'account') ?>",
                dataSrc: ""
            },
            rowId: 'pk_right',
            columns: [
                {
                    data: null,
                    orderable: false,
                    defaultContent: $('#actions').html(),
                    className: 'centered'
                },
                { data: 'name' }
            ],
            order: [[ 1, 'asc' ]]
        });
    });
</script>
<!-- SectionEnd -->
