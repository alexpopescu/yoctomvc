
<div class="row">
    <form class="form-horizontal" id="login-form" action="<?php echo $this->url->action('flow', 'do_login', null, 'account') ?>" method="post">
        <div id="forgot-password-errors" class="alert alert-danger" style="display: none;"></div>
        <div id="forgot-password-success" class="alert alert-success" style="display: none;"></div>

        <input type="hidden" name="redirect" value="<?php echo urldecode($this->requested); ?>" />

        <fieldset>
            <div class="form-group">
                <label class="col-md-3 control-label" for="login-form-email"><?php echo $this->language->get('email', 'account'); ?></label>
                <div class="col-md-9">
                    <input type="text" id="login-form-email" name="email" class="form-control" placeholder="<?php echo $this->language->get('enter_your_email', 'account'); ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label" for="login-form-password"><?php echo $this->language->get('password', 'account'); ?></label>
                <div class="col-md-9">
                    <input type="password" id="login-form-password" name="password" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="login-form-remember" name="remember" value="1"> <?php echo $this->language->get('remember_me', 'account'); ?>
                        </label>
                    </div>
                </div>
            </div>
        </fieldset>
        <button type="submit" class="btn btn-primary pull-right">
            <?php echo $this->language->get('sign_in', 'account'); ?>
        </button>
    </form>
</div>

<div class="row" style="margin-top: 2em;">
    <a href="<?php echo $this->url->action('flow', 'forgot', null, 'account'); ?>" class="pull-right">
        <?php echo $this->language->get('forgot_password', 'account'); ?></a><br/>
    <?php if ($this->config->get('account', 'allow_registration')) { ?>
    <a href="<?php echo $this->url->action('flow', 'register', null, 'account'); ?>" class="pull-right">
        <?php echo $this->language->get('register', 'account'); ?></a>
    <?php } ?>
</div>

