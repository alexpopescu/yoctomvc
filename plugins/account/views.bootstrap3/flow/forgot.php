
<div id="forgot-password-errors" class="alert alert-danger" style="display: none;"></div>
<div id="forgot-password-success" class="alert alert-success" style="display: none;"></div>

<form class="form-horizontal" id="forgot-password" action="<?php echo $this->url->action('flow', 'do_forgot_password', null, 'account') ?>">
    <fieldset>
        <div class="form-group">
            <label class="col-md-3 control-label" for="forgot-password-email"><?php echo $this->language->get('email', 'account'); ?></label>
            <div class="col-md-9">
                <input type="text" id="forgot-password-email" class="form-control" placeholder="<?php echo $this->language->get('enter_your_email', 'account'); ?>"/>
            </div>
        </div>
    </fieldset>
    <button id="forgot-password-submit" type="submit" class="btn btn-primary pull-right">
        <?php echo $this->language->get('submit'); ?>
    </button>
</form>

<!-- SectionStart: script -->
<script type="text/javascript">
    $(function(){
        $('#forgot-password-submit').on('click', function(){
            $.ajax({
                type: "POST",
                url: $('#forgot-password').attr('action'),
                data: {
                    email: $('#forgot-password-email').val()
                }
            }).done(function(data) {
                $('#forgot-password').find('div.form-group').removeClass('has-error');
                if (data.ok) {
                    $('#forgot-password').hide();
                    $('#forgot-password-success').html(data.msg).show();
                    $('#forgot-password-errors').hide();
                    return;
                }
                $('#forgot-password-errors').html(data.msg).show();
                if (data.fields && data.fields.length)
                    $.each(data.fields, function(index, value) {
                        $('#'+value).closest('div.form-group').addClass('has-error');
                    });
                });
            return false;
        });
    })
</script>
<!-- SectionEnd -->
