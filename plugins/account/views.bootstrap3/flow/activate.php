<?php if ($this->ok) { ?>
    <div id="register-success" class="alert alert-success"><?php echo $this->language->get('msg_activate_success', 'account'); ?></div>
<?php } else { ?>
    <div id="register-errors" class="alert alert-error"><?php echo $this->error; ?></div>
<?php } ?>
