<div id="register-errors" class="alert alert-danger" style="display: none;"></div>
<div id="register-success" class="alert alert-success" style="display: none;"></div>

<form class="form-horizontal" id="register-form" action="<?php echo $this->url->action('flow', 'do_register', null, 'account') ?>">
    <fieldset>
        <legend><?php echo $this->language->get('account_information', 'account'); ?></legend>
        <div class="form-group">
            <label class="col-md-3 control-label" for="register-email"><?php echo $this->language->get('email', 'account'); ?></label>
            <div class="col-md-9">
                <input type="text" id="register-email" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="register-password"><?php echo $this->language->get('password', 'account'); ?></label>
            <div class="col-md-9">
                <div class="input-group">
                    <input type="password" id="register-password" class="form-control"/>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" data-placement="bottom" data-toggle="popover" data-content="<?php echo $this->language->get('hint_password', 'account'); ?>">?</button>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label" for="register-confirm"><?php echo $this->language->get('confirm_password', 'account'); ?></label>
            <div class="col-md-9">
                <input type="password" id="register-confirm" class="form-control"/>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend><?php echo $this->language->get('personal_information', 'account'); ?></legend>

        <div class="form-group">
            <label class="col-md-3 control-label" for="register-first-name"><?php echo $this->language->get('first_name', 'account'); ?></label>
            <div class="col-md-9">
                <input type="text" id="register-first-name" class="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="register-last-name"><?php echo $this->language->get('last_name', 'account'); ?></label>
            <div class="col-md-9">
                <input type="text" id="register-last-name" class="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="register-url"><?php echo $this->language->get('website', 'account'); ?></label>
            <div class="col-md-9">
                <input type="text" id="register-url" class="form-control"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="register-timezone"><?php echo $this->language->get('timezone'); ?></label>
            <div class="col-md-9">
                <select id="register-timezone" class="form-control"
                        data-url="<?php echo $this->url->action('global', 'timezones') ?>"
                        data-value="pk_timezone" data-name="name" data-nullable="1"
                        data-group-name="name" data-group-content="zones"></select>
            </div>
        </div>
    </fieldset>
    <div class="form-group">
        <div class="col-12">
            <button id="register-submit" type="submit" class="btn btn-primary pull-right">
                <?php echo $this->language->get('submit'); ?>
            </button>
        </div>
    </div>
</form>

<!-- SectionStart: script -->
<script type="text/javascript">
    $(function() {
        $('#register-submit').on('click', function () {
            $.ajax({
                type: "POST",
                url: $('#register-form').attr('action'),
                data: {
                    password: $('#register-password').val(),
                    confirm: $('#register-confirm').val(),
                    email: $('#register-email').val(),
                    firstname: $('#register-first-name').val(),
                    lastname: $('#register-last-name').val(),
                    url: $('#register-url').val(),
                    timezone: $('#register-timezone').val()
                }
            }).done(function (data) {
                $('#register-form div.form-group').removeClass('has-error');
                if (data.ok) {
                    $('#register-form').hide();
                    $('#register-success').html(data.msg).show();
                    $('#register-errors').hide();
                    return;
                }
                $('#register-errors').html(data.msg).show();
                if (data.fields && data.fields.length)
                    $.each(data.fields, function (index, value) {
                        $('#' + value).closest('div.form-group').addClass('has-error');
                    });
            });
            return false;
        });
    });
</script>
<!-- SectionEnd -->
