
<form class="form-horizontal" id="user-profile-form" action="<?php echo $this->url->action('user', 'save_profile', null, 'account') ?>" method="post"
      data-success="[data-alert=&quot;success&quot;]" data-error="[data-alert=&quot;error&quot;]"
      data-source="<?php echo $this->url->action('user', 'get', null, 'account') ?>">

    <input type="hidden" id="user-profile-id" name="pk_user" />

    <fieldset>
        <legend><?php echo $this->language->get('basic_information', 'account'); ?></legend>

        <div class="form-group">
            <label class="col-md-3 control-label" for="user-profile-email"><?php echo $this->language->get('email', 'account'); ?></label>
            <div class="col-md-9">
                <input type="text" id="user-profile-email" name="email" class="form-control"<?php echo ($this->config->get('account', 'allow_changing_email') ? '' : ' readonly'); ?> />
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="user-profile-first_name"><?php echo $this->language->get('first_name', 'account'); ?></label>
            <div class="col-md-9">
                <input type="text" id="user-profile-first_name" name="first_name" class="form-control" />
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="user-profile-last_name"><?php echo $this->language->get('last_name', 'account'); ?></label>
            <div class="col-md-9">
                <input type="text" id="user-profile-last_name" name="last_name" class="form-control" />
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="user-profile-url"><?php echo $this->language->get('website', 'account'); ?></label>
            <div class="col-md-9">
                <input type="text" id="user-profile-url" name="url" class="form-control" />
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="user-profile-timezone"><?php echo $this->language->get('timezone'); ?></label>
            <div class="col-md-9">
                <select id="user-profile-timezone" name="fk_timezone" class="form-control"
                        data-url="<?php echo $this->url->action('global', 'timezones') ?>"
                        data-value="pk_timezone" data-name="name" data-nullable="1"
                        data-group-name="name" data-group-content="zones"></select>
            </div>
        </div>

    </fieldset>

    <fieldset>
        <legend><?php echo $this->language->get('change_password', 'account'); ?></legend>

        <div class="form-group">
            <label class="col-md-3 control-label" for="user-profile-password"><?php echo $this->language->get('password', 'account'); ?></label>
            <div class="col-md-9">
                <input type="password" id="user-profile-password" name="password" class="form-control" />
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="user-profile-confirm"><?php echo $this->language->get('confirm_password', 'account'); ?></label>
            <div class="col-md-9">
                <input type="password" id="user-profile-confirm" name="confirm" class="form-control" />
            </div>
        </div>

    </fieldset>

    <div class="form-group">
        <div class="col-12">
            <button data-submit="#user-profile-form" type="button" class="btn btn-primary pull-right"><?php echo $this->language->get('save'); ?></button>
        </div>
    </div>

</form>


