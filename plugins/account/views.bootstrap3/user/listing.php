
<table id="users" class="table table-striped table-bordered" cellspacing="0" width="100%"
       data-editor="#user-edit" data-success="[data-alert=&quot;success&quot;]" data-error="[data-alert=&quot;error&quot;]">
    <thead>
        <tr>
            <th><span class="glyphicon glyphicon-plus text-success" title="<?php echo $this->language->get('add_user_title', 'account'); ?>"></span></th>
            <th><?php echo $this->language->get('first_name', 'account'); ?></th>
            <th><?php echo $this->language->get('last_name', 'account'); ?></th>
            <th><?php echo $this->language->get('email', 'account'); ?></th>
            <th><?php echo $this->language->get('status', 'account'); ?></th>
            <th><?php echo $this->language->get('create_date', 'account'); ?></th>
            <th><?php echo $this->language->get('update_date', 'account'); ?></th>
        </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
        <tr>
            <th></th>
            <th><?php echo $this->language->get('first_name', 'account'); ?></th>
            <th><?php echo $this->language->get('last_name', 'account'); ?></th>
            <th><?php echo $this->language->get('email', 'account'); ?></th>
            <th><?php echo $this->language->get('status', 'account'); ?></th>
            <th><?php echo $this->language->get('create_date', 'account'); ?></th>
            <th><?php echo $this->language->get('update_date', 'account'); ?></th>
        </tr>
    </tfoot>
</table>


<div id="user-edit" class="modal fade" data-keyboard="true"
     data-get-url="<?php echo $this->url->action('user', 'get', null, 'account') ?>"
     data-add-title="<?php echo $this->language->get('add_user_title', 'account'); ?>"
     data-edit-title="<?php echo $this->language->get('edit_user_title', 'account'); ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="user-edit-form" action="<?php echo $this->url->action('user', 'save', null, 'account') ?>" method="post"
                      data-success="[data-alert=&quot;success&quot;]" data-error="[data-alert=&quot;form-error&quot;]">

                    <input type="hidden" id="user-edit-id" name="pk_user" />
                    <div data-alert="form-error" class="alert alert-danger" style="display: none;"></div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="user-edit-email"><?php echo $this->language->get('email', 'account'); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="user-edit-email" name="email" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="user-edit-first_name"><?php echo $this->language->get('first_name', 'account'); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="user-edit-first_name" name="first_name" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="user-edit-last_name"><?php echo $this->language->get('last_name', 'account'); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="user-edit-last_name" name="last_name" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="user-edit-url"><?php echo $this->language->get('website', 'account'); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="user-edit-url" name="url" class="form-control" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="user-edit-status"><?php echo $this->language->get('status', 'account'); ?></label>
                        <div class="col-md-9">
                            <select id="user-edit-status" name="status" class="form-control"
                                    data-url="<?php echo $this->url->action('user', 'get_all_statuses', null, 'account'); ?>"></select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="user-edit-timezone"><?php echo $this->language->get('timezone'); ?></label>
                        <div class="col-md-9">
                            <select id="user-edit-timezone" name="fk_timezone" class="form-control"
                                    data-url="<?php echo $this->url->action('global', 'timezones'); ?>"
                                    data-value="pk_timezone" data-name="name" data-nullable="1"
                                    data-group-name="name" data-group-content="zones"></select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="user-edit-roles"><?php echo $this->language->get('roles', 'account'); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="user-edit-roles" name="roles" class="form-control" data-role="tags"
                                   data-source="<?php echo $this->url->action('auth', 'get_all_roles', null, 'account') ?>"
                                   data-identity="pk_role" data-display="name" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="user-edit-rights"><?php echo $this->language->get('rights', 'account'); ?></label>
                        <div class="col-md-9">
                            <input type="text" id="user-edit-rights" name="rights" class="form-control" data-role="tags"
                                   data-source="<?php echo $this->url->action('auth', 'get_all_rights', null, 'account') ?>"
                                   data-identity="pk_right" data-display="name" />
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->language->get('cancel'); ?></button>
                <button data-submit="#user-edit-form" type="button" class="btn btn-primary"><?php echo $this->language->get('save'); ?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- actions template -->
<div id="actions" style="display: none;">
    <span class="glyphicon glyphicon-edit" title="<?php echo $this->language->get('edit_user_title', 'account'); ?>"></span>
    <span class="glyphicon glyphicon-remove text-danger"
          data-url = "<?php echo $this->url->action('user', 'delete', null, 'account') ?>"
          data-confirm = "<?php echo $this->language->get('confirmation'); ?>"
          title="<?php echo $this->language->get('delete_user_title', 'account'); ?>"></span>
</div>


<!-- SectionStart: header -->
<style type="text/css">
    tr.deleted {
        color: #CCC;
    }
    tr.deleted .glyphicon {
        display: none;
    }
</style>
<!-- SectionEnd -->

<!-- SectionStart: script -->
<script type="application/javascript">
    $(document).ready(function() {

        // initialize data table
        $('#users').DataTable({
            deferRender: true,
            processing: true,
            ajax: {
                url: "<?php echo $this->url->action('user', 'get_all', null, 'account') ?>",
                dataSrc: ""
            },
            rowId: 'pk_user',
            columns: [
                {
                    data: null,
                    orderable: false,
                    defaultContent: $('#actions').html(),
                    className: 'centered'
                },
                { data: 'first_name' },
                { data: 'last_name' },
                { data: 'email' },
                { data: 'status' },
                {
                    data: 'create_date',
                    render: {
                        _: 'timestamp',
                        filter: 'display',
                        display: 'display'
                    }
                },
                {
                    data: 'update_date',
                    render: {
                        _: 'timestamp',
                        filter: 'display',
                        display: 'display'
                    }
                }
            ],
            order: [[ 1, 'asc' ]],
            createdRow: function( row, data, dataIndex ) {
                $(row).addClass( data.status );
            }
        });
    });
</script>
<!-- SectionEnd -->
