<li class="dropdown">
    <?php if ($this->session->get('account', 'pk_user') > 0) { ?>
        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->session->get('account', 'name'); ?><b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a tabindex="-1" href="<?php echo $this->url->action('user', 'profile', null, 'account') ?>"><?php echo $this->language->get('my_profile', 'account'); ?></a></li>
            <li class="divider"></li>
            <li><a tabindex="-1" href="<?php echo $this->url->action('flow', 'logout', null, 'account') ?>" id="btn-account-logout"><?php echo $this->language->get('sign_out', 'account'); ?></a></li>
        </ul>
    <?php } else { ?>
        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
            <?php echo $this->language->get('sign_in_title', 'account'); ?><b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li style="padding: 10px; overflow: hidden;">
                <form method="post" action="<?php echo $this->url->action('flow', 'do_login', null, 'account') ?>" id="login-box">
                    <input type="hidden" name="redirect" value="<?php echo urldecode($this->requested); ?>" />

                    <div class="alert alert-danger" id="login-box-error"></div>
                    <div class="form-group">
                        <input type="text" id="login-box-email" name="email" class="form-control" placeholder="<?php echo $this->language->get('email', 'account'); ?>"/>
                    </div>
                    <div class="form-group">
                        <input type="password" id="login-box-password" name="password" class="form-control" placeholder="<?php echo $this->language->get('password', 'account'); ?>"/>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" id="login-box-remember" name="remember" value="1" />
                        <label for="login-box-remember"><?php echo $this->language->get('remember_me', 'account'); ?></label>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right" id="login-box-submit">
                        <?php echo $this->language->get('sign_in', 'account'); ?>
                    </button>
                </form>
            </li>
            <li class="divider" style="clear: both;"></li>
            <li><a tabindex="-1" href="<?php echo $this->url->action('flow', 'forgot', null, 'account') ?>">
                    <?php echo $this->language->get('forgot_password', 'account'); ?></a></li>
            <?php if ($this->config->get('account', 'allow_registration')) { ?>
            <li><a tabindex="-1" href="<?php echo $this->url->action('flow', 'register', null, 'account') ?>">
                    <?php echo $this->language->get('register', 'account'); ?></a></li>
            <?php } ?>
        </ul>
    <?php } ?>
</li>
