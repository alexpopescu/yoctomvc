<?php
namespace NeuralToys\YoctoMVC\Plugins\Blog\Controllers {
    use Exception;
    use NeuralToys\YoctoMVC\Core\Controller;
    use NeuralToys\YoctoMVC\Core\Parameters;
    use NeuralToys\YoctoMVC\Plugins\Blog\Models\Post_Model;

    /**
     * This file handles the posts
     */
    class Post_Controller extends Controller {
        private $limit = 10;

        public $helpers = array('html', 'url');

        /**
         * The post listing
         * @param Parameters $params
         */
        public function index(Parameters $params) {
            $model = new Post_Model($this->config);
            $data = $model->listing(
                property_exists($params, 'offset') ? $params->offset : 0,
                property_exists($params, 'limit') ? $params->limit : $this->limit);
            for ($i = 0; $i < count($data); $i++) {
                $data[$i]['edit'] = $data[$i]['user_id'] == $this->session->get('account', 'id');
                $data[$i]['delete'] = $data[$i]['edit'] || $this->session->has_rights('blog_post_delete');
            }
            $this->view(array('list' => $data));
        }

        /**
         * View one post
         * @param Parameters $params
         */
        public function read(Parameters $params) {
            //if (empty($params->id)) return $this->redirect('/blog/post');
            $model = new Post_Model($this->config);
            $data = $model->get_by_guid($params->id, $this->session->get('account', 'timezone'));
            if (!empty($data['post']['publish_date']))
                $data['post']['publish_date_str'] = $data['post']['publish_date']->format('d/m/Y H:i:s');
            else $data['post']['publish_date_str'] = $this->language->get('post_status_draft', 'blog');
            for ($i = 0; $i < count($data['comments']); $i++)
                $data['comments'][$i]['create_date_str'] = $data['comments'][$i]['create_date']->format('d/m/Y H:i:s');
            $data['edit'] = $data['post']['user_id'] == $this->session->get('account', 'id');
            $data['delete'] = $data['edit'] || $this->session->has_rights('blog_post_delete');
            //if ($data['edit'])
            //    $data['translations'] = $model->get_translations($data['post']['id']);
            $this->view($data);
        }

        /**
         * Add/Edit post modal dialog
         * @param Parameters $params
         */
        public function edit(Parameters $params) {
            $this->partial_view($params);
        }

        /**
         * Returns the data for a post
         * @param Parameters $params
         */
        public function get(Parameters $params) {
            $this->validate_method('post');
            try{
                //if (!$params->id) return Router::http_error(404);
                $model = new Post_Model($this->config);
                $post = $model->get($params->id);
                //if (empty($post) || $post['user_id'] != $this->session->get('account', 'id'))
                //    return Router::http_error(404);
                $translations = $model->get_translations($params->id);
                $this->json(array(
                    'ok' => true,
                    'post' => $post,
                    'translations' => $translations
                ));
            } catch (Exception $e) {
                $this->json_error($e->getMessage());
            }
        }

        /**
         * Save post
         * @param Parameters $params
         */
        public function save(Parameters $params) {
            $this->validate_method('post');
            dump($params);
            exit;
            try{
                $model = new Post_Model($this->config);
                $model->save($this->session->get('account', 'id'), $params->id, $params->status, $params->comment_status, $params->translations, $params->reason);
                $this->json_success($this->language->get('msg_post_saved', 'blog'));
            } catch (Exception $e) {
                $this->json_error($e->getMessage());
            }
        }

        /**
         * Delete post
         * @param Parameters $params
         */
        public function delete(Parameters $params) {
            $this->validate_method('post');
            try{
                $model = new Post_Model($this->config);
                if ($model->delete_post($params->id))
                    $this->json_success($this->language->get('msg_post_deleted', 'blog'));
                else $this->json_error($this->language->get('msg_post_delete_failed', 'blog'));
            } catch (Exception $e) {
                $this->json_error($e->getMessage());
            }
        }
    }
}
