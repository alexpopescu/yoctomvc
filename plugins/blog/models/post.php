<?php
namespace NeuralToys\YoctoMVC\Plugins\Blog\Models {
    use NeuralToys\YoctoMVC\Core\Model;
    use NeuralToys\YoctoMVC\Tools\Date_Tool;
    use PDO;

    /**
     * The Post Model does the back-end work for the Post Controller
     */
    class Post_Model extends Model {

        /**
         * Get the list of posts
         * @param $offset int The number of posts to skip
         * @param $limit int The number of posts to return
         * @return array A collection of posts
         */
        public function listing($offset, $limit) {
            $lang = 'it';
            $sql = '
        SELECT bp.*,
          COALESCE(bpl.`title`, bpld.`title`) AS `title`,
          COALESCE(bpl.`title_normalized`, bpld.`title_normalized`) AS `guid`,
          COALESCE(bpl.`content`, bpld.`content`) AS `content`,
          COALESCE(bc.`comments`, 0) AS `comments`,
          COALESCE(CONCAT(au.`first_name`, " ", au.`last_name`), au.`username`) AS `author`
        FROM `blog_post` bp
        LEFT JOIN `blog_post_lang` bpl ON bp.`id` = bpl.`post_id` AND bpl.`lang_code` = :lang
        LEFT JOIN `blog_post_lang` bpld ON bp.`id` = bpld.`post_id` AND bpld.`lang_code` = :def_lang
        LEFT JOIN (SELECT COUNT(1) AS `comments`, `post_id` FROM `blog_comment` GROUP BY `post_id`) bc ON bp.`id` = bc.`post_id`
        LEFT JOIN `account_user` au ON bp.`user_id` = au.`id`
        WHERE bp.`status` = "published"
        ORDER BY bp.`publish_date` DESC
        LIMIT :offset,:limit';

            $db = parent::connect();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':lang', $lang, PDO::PARAM_STR);
            $def_lang = $this->config->get('core', 'defaults', 'language');
            $stmt->bindParam(':def_lang', $def_lang, PDO::PARAM_STR);
            $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
            $stmt->execute();

            $ret = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $ret;
        }

        public function get_by_guid($guid, $timezone) {
            parent::connect();
            $post = $this->proc_get_row('blog_post_get_by_title', array(
                'p_normalized_title' => $guid
            ));

            $comments = null;
            if (!empty($post)) {
                $post['create_date'] = Date_Tool::timestamp_to_date($post['create_date'], $timezone);
                $post['update_date'] = Date_Tool::timestamp_to_date($post['update_date'], $timezone);
                if (!empty($post['publish_date']))
                    $post['publish_date'] = Date_Tool::timestamp_to_date($post['publish_date'], $timezone);

                $comments = $this->proc_get_list('blog_comment_get_by_post', array(
                    'p_post_id' => intval($post['id'])
                ));
                if (!empty($comments))
                    for ($i = 0; $i < count($comments); $i++)
                        $comments[$i]['create_date'] = Date_Tool::timestamp_to_date($comments[$i]['create_date'], $timezone);
            }

            return array(
                'post' => $post,
                'comments' => $comments
            );
        }

        public function get($post_id) {
            parent::connect();
            return $this->proc_get_row('blog_post_get', array(
                'p_id' => $post_id
            ));
        }

        public function get_translations($post_id) {
            parent::connect();
            return $this->proc_get_list('blog_post_lang_get_by_post', array(
                'p_post_id' => $post_id
            ));
        }

        /**
         * Creates or updates a blog post
         * @param int $pk_user
         * @param $id int The post id
         * @param $status string The post status draft/published/hidden
         * @param $accept_comments bool Whether the post accepts comments or not
         * @param null $languages array An array containing all the possible translations of this post
         * @param null $reason string If this is an update this will contain the reason of the update
         * @return int The id of the blog post
         */
        public function save($pk_user, $id, $status, $accept_comments, $languages = null, $reason = null) {
            parent::connect();
            $res = $this->proc_get_row('blog_post_save', array(
                'p_id' => empty($id) || $id <= 0 ? null : $id,
                'p_user_id' => $pk_user,
                'p_status' => $status,
                'p_comment_status' => $accept_comments ? 'open' : 'closed'
            ));
            $id = intval($res['id']);
            if (!empty($languages)) {
                $revision_id = null;
                foreach ($languages AS $lang)
                    $revision_id = $this->save_lang($pk_user, $id, $lang['code'], $lang['title'], $lang['content'], $revision_id, $reason);
            }
            return $id;
        }

        /**
         * Creates or updates a post translation
         * @param int $pk_user
         * @param $post_id int The post id
         * @param $lang string The language code
         * @param $title string The title of the blog post
         * @param $content string The content of the blog post
         * @param null $revision_id int The revision id, if available
         * @param null $reason string The reason for updating, if this is a revision
         * @return int The revision id, if any, otherwise null
         */
        public function save_lang($pk_user, $post_id, $lang, $title, $content, $revision_id = null, $reason = null) {
            parent::connect();
            $res = $this->proc_get_row('blog_post_lang_save', array(
                'p_post_id' => $post_id,
                'p_lang_code' => $lang,
                'p_title' => $title,
                'p_title_normalized' => extract_permalink($title),
                'p_content' => $content,
                'p_user_id' => $pk_user,
                'p_revision_reason' => $reason,
                'p_revision_id' => $revision_id
            ));
            return empty($res['revision_id']) ? null : intval($res['revision_id']);
        }

        /**
         * Deletes a post
         * @param $post_id int The id of the post to delete
         * @return bool true if the statement succeeded, false otherwise
         */
        public function delete_post($post_id) {
            parent::connect();
            return $this->proc_exec('blog_post_get', array(
                'p_id' => $post_id
            ));
        }

    }
}