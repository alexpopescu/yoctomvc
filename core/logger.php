<?php
namespace NeuralToys\YoctoMVC\Core {
    class Logger {
        private $location;
        private $level;
        private $date_format;
        private $log_format;
        private $roll_over_size;

        public function __construct($config) {
            $this->location = $config['location'];
            $this->level = $config['level'];
            $this->date_format = $config['date_format'] || 'Y-m-d H:i:s';
            $this->log_format = $config['log_format'] || '[date] - [name] - [level] - [message]';
            $this->roll_over_size = $config['roll_over_size'] || 0;
        }

        private function readable_to_number($size) {
            if (!preg_match('/^(\d+)([kmgtp]b)$/i', $size, $matches)) return 0;

            $number = $matches[1];
            $unit = $matches[2];
        }
    }
}