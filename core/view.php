<?php
namespace NeuralToys\YoctoMVC\Core {
    use Exception;

    /**
     * Handles the view functionality of our MVC framework
     */
    final class View_Handler
    {
        private $config;
        private $language;
        private $plugin_name;
        private $layout_name;
        private $template_name;

        /**
         * Holds variables assigned to template
         */
        private $data = array();

        public function __set($name, $value) {
            $this->data[$name] = $value;
        }

        public function __get($name) {
            if (array_key_exists($name, $this->data))
                return $this->data[$name];

            return null;
        }

        /**
         * Holds render status of view.
         */
        private $view = FALSE;
        private $layout = FALSE;
        private $sections = array();

        public $content = null;

        /**
         * Accept a template to load
         * @param Configuration $config
         * @param Language $language
         * @param $template
         * @param string $layout
         * @param string $plugin
         */
        public function __construct(Configuration $config, Language $language, $template, $layout, $plugin) {
            $this->config = $config;
            $this->language = $language;

            if ($layout === '') $layout = $this->config->get('core', 'defaults', 'layout');
            $this->layout_name = strtolower(trim($layout));
            $this->template_name = strtolower(trim($template));
            $this->plugin_name = strtolower(trim($plugin));
        }

        /**
         * Add a helper instance to the view
         * @param $name
         * @param $instance
         */
        public function create_helper($name, $instance) {
            $this->$name = $instance;
        }

        /**
         * Receives assignments from controller and stores in local data array
         *
         * @param $variable
         * @param $value
         */
        public function assign($variable, $value = null)
        {
            if (!$variable) return;
            if (is_array($variable)) {
                foreach ($variable as $key => $value)
                    $this->assign($key, $value);
                return;
            }
            if (is_object($variable)) {
                $this->assign(get_object_vars($variable));
                return;
            }
            if ($value == null) return;
            $this->data[$variable] = $value;
        }

        private function find_files_to_render() {
            $root_folder = $this->config->get('path', 'root');
            $plugins_folder = $this->config->get('path', 'plugins');
            $renderer = $this->config->get('core', 'renderer');
            $current_language = $this->language->code();

            //compose file name
            if ($this->plugin_name) {
                $plugin_folder = $plugins_folder . DS . $this->plugin_name;
                $files = array(
                    $plugin_folder . DS . 'views.' . $renderer . DS . $this->template_name . '.' . $current_language . '.php',
                    $plugin_folder . DS . 'views.' . $renderer . DS . $this->template_name . '.php',
                    $plugin_folder . DS . 'views.' . $renderer . DS . $this->template_name . '.' . $current_language . '.example.php',
                    $plugin_folder . DS . 'views.' . $renderer . DS . $this->template_name . '.example.php',
                    $plugin_folder . DS . 'views' . DS . $this->template_name . '.' . $current_language . '.php',
                    $plugin_folder . DS . 'views' . DS . $this->template_name . '.php',
                    $plugin_folder . DS . 'views' . DS . $this->template_name . '.' . $current_language . '.example.php',
                    $plugin_folder . DS . 'views' . DS . $this->template_name . '.example.php'
                );
            } else {
                $files = array(
                    $root_folder . DS . 'views.' . $renderer . DS . $this->template_name . '.' . $current_language . '.php',
                    $root_folder . DS . 'views.' . $renderer . DS . $this->template_name . '.php',
                    $root_folder . DS . 'views.' . $renderer . DS . $this->template_name . '.' . $current_language . '.example.php',
                    $root_folder . DS . 'views.' . $renderer . DS . $this->template_name . '.example.php',
                    $root_folder . DS . 'views' . DS . $this->template_name . '.' . $current_language . '.php',
                    $root_folder . DS . 'views' . DS . $this->template_name . '.php',
                    $root_folder . DS . 'views' . DS . $this->template_name . '.' . $current_language . '.example.php',
                    $root_folder . DS . 'views' . DS . $this->template_name . '.example.php'
                );
            }
            $this->view = get_first_available_file($files, $root_folder . DS . 'views' . DS . 'global' . DS . 'missing.php');

            if (!empty($this->layout_name)) {
                $this->layout = get_first_available_file(array(
                    $root_folder . DS . 'views.' . $renderer . DS . $this->layout_name . '.php',
                    $root_folder . DS . 'views.' . $renderer . DS . $this->layout_name . '.example.php',
                    $root_folder . DS . 'views' . DS . $this->layout_name . '.php',
                    $root_folder . DS . 'views' . DS . $this->layout_name . '.example.php'
                ));
            }
        }

        /**
         * Render the output directly to the page, or optionally, return the
         * generated output to caller.
         *
         * @param bool $direct_output Set to any non-TRUE value to have the output returned rather than displayed directly.
         * @return string the output if $direct_output is set to TRUE, nothing otherwise
         */
        public function render($direct_output = TRUE) {
            $this->find_files_to_render();

            // load the view
            ob_start(null, 0, PHP_OUTPUT_HANDLER_STDFLAGS ^ PHP_OUTPUT_HANDLER_CLEANABLE);
            include($this->view);
            $this->content = ob_get_clean();

            // search for sections and remove them from the view
            if (preg_match_all('/<!--[\s\t]*SectionStart:[\s\t]*(\w+)[\s\t]*-->(.*)<!--[\s\t]*SectionEnd[\s\t]*-->/imsU', $this->content, $matches))
                for ($i = 0; $i < sizeof($matches[0]); $i++) {
                    $sname = trim($matches[1][$i]);
                    if (!$sname) continue;
                    $this->sections[$sname] = trim($matches[2][$i]);
                    $this->content = str_replace($matches[0][$i], '', $this->content);
                }

            if ($this->layout === false) {
                if ($direct_output === TRUE) echo $this->content;
                else return $this->content;
                return null;
            }

            // load the layout
            if ($direct_output !== TRUE) ob_start(null, 0, PHP_OUTPUT_HANDLER_STDFLAGS ^ PHP_OUTPUT_HANDLER_CLEANABLE);
            include($this->layout);
            if ($direct_output !== TRUE) return ob_get_clean();
            return null;
        }

        /**
         * Outputs a specific section
         * @param string $name the name of the section
         * @param bool $optional whether the section is optional or not
         * @throws Exception
         */
        public function section($name, $optional = false) {
            if (!isset($this->sections[$name])){
                if (!$optional) throw new Exception('Mandatory section not found in view: '.$name);
                return;
            }
            echo $this->sections[$name];
        }

        public function menu($align) {
            $plugins = $this->config->get('core', 'plugins');
            $plugins_folder = $this->config->get('path', 'plugins');
            $renderer = $this->config->get('core', 'renderer');
            foreach ($plugins as $plugin) {
                $plugin_conf = $this->config->get($plugin, 'menu');
                if ($plugin_conf == null || empty($plugin_conf['view']) || empty($plugin_conf['align']) || $plugin_conf['align'] != $align) continue;
                $file = get_first_available_file(array(
                    $plugins_folder . DS . $plugin . DS . 'views.' . $renderer . DS . $plugin_conf['view'],
                    $plugins_folder . DS . $plugin . DS . 'views' . DS . $plugin_conf['view']
                ));
                if ($file != null) include($file);
            }
        }
    }
}
