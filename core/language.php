<?php
namespace NeuralToys\YoctoMVC\Core {

    class Language
    {
        private $config = null;
        private $currentCode = null;
        private $current = array();
        private $defaultCode = null;
        private $default = array();

        public function __construct(Configuration $config, $current) {
            $this->config = $config;
            $this->currentCode = $current;
            $this->defaultCode = $this->config->get('core', 'defaults', 'language');
            if (!empty($this->currentCode) && $this->currentCode != $this->defaultCode)
                $this->current = $this->load($this->currentCode);
            $this->default = $this->load($this->defaultCode);
        }

        private function load($language) {
            // load core files
            $folder = $this->config->get('path', 'root') . DS . 'language' . DS . '*.' . $language . '.ini';
            $files = glob($folder);
            $ret = array();
            foreach ($files as $file)
                $ret = array_merge_recursive($ret, parse_ini_file($file));
            // load plugin files
            $plugins = $this->config->get('core', 'plugins');
            foreach ($plugins as $plugin) {
                $ret[$plugin] = array();
                $folder = $this->config->get('path', 'plugins') . DS . $plugin . DS . 'language' . DS . '*.' . $language . '.ini';
                $files = glob($folder);
                foreach ($files as $file)
                    $ret[$plugin] = array_merge_recursive($ret[$plugin], parse_ini_file($file));
            }
            return $ret;
        }

        public function code() {
            return $this->currentCode ?: $this->defaultCode;
        }

        public function get($key, $plugin = null) {
            if (empty($plugin)) {
                if (isset($this->current[$key])) return $this->current[$key];
                if (isset($this->default[$key])) return $this->default[$key];
            }
            if (isset($this->current[$plugin][$key])) return $this->current[$plugin][$key];
            if (isset($this->default[$plugin][$key])) return $this->default[$plugin][$key];
            return '### resource not found: '.$key.' ###';
        }
    }
}