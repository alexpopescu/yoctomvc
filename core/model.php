<?php
namespace NeuralToys\YoctoMVC\Core {
    use PDO;

    /**
     * The Base Model is the class that should be inherited by all models
     */
    abstract class Model {

        /** @var PDO */
        protected $db = null;

        /** @var Configuration */
        protected $config;

        public function __construct(Configuration $config) {
            $this->config = $config;
        }

        public function __destruct() {
            $this->disconnect();
        }

        protected function connect() {
            $db_config = $this->config->get('core', 'database');
            if ($db_config == null || empty($db_config['connection'])) return null;
            $this->db = new PDO(
                $db_config['connection'],
                $db_config['username'],
                $db_config['password'],
                array(
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_STRINGIFY_FETCHES => false
                ));
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $this->db;
        }

        protected function disconnect() {
            $this->db = null;
        }

        protected final function param_type($param) {
            if ($param === null) return PDO::PARAM_NULL;
            if (is_integer($param)) return PDO::PARAM_INT;
            if (is_bool($param)) return PDO::PARAM_BOOL;
            return PDO::PARAM_STR;
        }

        /**
         * Prepare a statement for execution on the database
         * @param string $sql The query to execute
         * @param array $params An array of parameters for the query
         * @return mixed
         */
        protected function statement($sql, $params = array()) {
            if (empty($this->db)) $this->connect();
            $stmt = $this->db->prepare($sql);
            foreach ($params as $key => $value) {
                $stmt->bindValue(':'.$key, $value, $this->param_type($value));
            }
            return $stmt;
        }

        /**
         * Execute a query on the database and return the result
         * @param string $sql The query to execute
         * @param array $params An array of parameters for the query
         * @param int $fetchMode The type of data structure to return
         * @param string $className In case objects need to be returned, this specifies which type to use
         * @return mixed|null The collection of result rows if any
         */
        protected function query($sql, $params = array(), $fetchMode = PDO::FETCH_ASSOC, $className = '') {
            $stmt = $this->statement($sql, $params);
            if (!$stmt->execute()) return null;

            if ($fetchMode === PDO::FETCH_CLASS) {
                return $stmt->fetchAll($fetchMode, $className);
            }
            return $stmt->fetchAll($fetchMode);
        }

        /**
         * Execute a query on the database and return one single row
         * @param string $sql The query to execute
         * @param array $params An array of parameters for the query
         * @param int $fetchMode The type of data structure to return
         * @return mixed|null The first returned row if any
         */
        protected function single($sql, $params = array(), $fetchMode = PDO::FETCH_ASSOC) {
            $stmt = $this->statement($sql, $params);
            if (!$stmt->execute()) return null;
            return $stmt->fetch($fetchMode);
        }

        /**
         * Execute a database query without response
         * @param string $sql The query to execute
         * @param array $params An array of parameters for the query
         * @return mixed
         */
        protected function execute($sql, $params = array()) {
            $stmt = $this->statement($sql, $params);
            return $stmt->execute();
        }

        /**
         * Insert a record into a table in the database
         * @param string $table The name of the table
         * @param array $data The array of data to insert
         * @return mixed|null If successful it will return the id of the last inserted row, otherwise null
         */
        protected function insert($table, $data) {
            $keys = array_keys($data);
            $fieldNames = implode(',', $keys);
            $fieldValues = ':'.implode(', :', $keys);

            $sql = 'INSERT INTO `'.$table.'` ('.$fieldNames.') VALUES ('.$fieldValues.')';
            if ($this->execute($sql, $data)) return $this->db->lastInsertId();
            return null;
        }
        /**
         * Update a record into a table in the database
         * @param string $table The name of the table
         * @param array $data The array of data to update
         * @param array $conditions The array of conditions to check for
         * @return mixed|null If successful it will return the number of affected rows, otherwise null
         */
        protected function update($table, $data, $conditions) {
            $set = '';
            $where = '';
            $params = array();

            foreach ($data as $key => $value) {
                $set .= (strlen($set) > 0 ? ', ' : '').'`'.$key.'` = :field_'.$key;
                $params['field_'.$key] = $value;
            }
            foreach ($conditions as $key => $value) {
                $where .= (strlen($where) > 0 ? ' AND ' : '').'`'.$key.'` = :where_'.$key;
                $params['where_'.$key] = $value;
            }

            $sql = 'UPDATE `'.$table.'` SET '.$set.' WHERE '.$where;
            $stmt = $this->statement($sql, $params);
            if ($stmt->execute()) return $stmt->rowCount();
            return null;
        }

        /**
         * Delete one or more records from a table in the database
         * @param string $table The name of the table
         * @param array $conditions The array of conditions to check for
         * @param int $limit The maximum number of records to delete
         * @return mixed|null If successful it will return the number of affected rows, otherwise null
         */
        protected function delete($table, $conditions, $limit = 1) {
            $where = '';
            foreach ($conditions as $key => $value) $where .= (strlen($where) > 0 ? ' AND ' : '').'`'.$key.'` = :'.$key;

            $sql = 'DELETE FROM `'.$table.'` WHERE '.$where.(is_numeric($limit) ? ' LIMIT '.$limit : '');
            $stmt = $this->statement($sql, $conditions);
            if ($stmt->execute()) return $stmt->rowCount();
            return null;
        }
    }
}