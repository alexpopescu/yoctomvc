<?php
namespace NeuralToys\YoctoMVC\Core {
    use Exception;

    /**
     * Custom exception to be used for all data problems
     */
    class DataException extends Exception {
        // custom string representation of object
        public function __toString() {
            return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        }
    }
}

