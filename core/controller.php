<?php
namespace NeuralToys\YoctoMVC\Core {
    use Externals;

    /**
     * This is the base controller, that should be inherited by all controllers
     */
    abstract class Controller {
        /**
         * @var Configuration Contains the complete configuration of the site
         */
        protected $config;
        /**
         * @var Language Handles localization management
         */
        protected $language;
        /**
         * @var /Logger The logger object available for this request
         */
        protected $logger;
        /**
         * @var array An array containing all instantiated helper classes
         */
        public $helpers = array();

        /**
         * Creates a controller instance
         * @param Configuration $config The loaded configuration
         * @param string $languageCode The current language
         */
        public function __construct(Configuration $config, $languageCode) {
            $this->config = $config;

            // initialize helpers
            if (!in_array('url', $this->helpers)) $this->helpers[] = 'url';
            if (!in_array('html', $this->helpers)) $this->helpers[] = 'html';
            if (!in_array('session', $this->helpers)) $this->helpers[] = 'session';
            foreach ($this->helpers as $helper) $this->create_helper($helper);

            // start session
            $this->session->start($this->config->get('core', 'session', 'id'));

            // initialize language
            $this->language = new Language($this->config, $languageCode);

            // initialize logger instance
            Externals::load('log4php');
            $class = get_class($this);
            $this->logger = Externals::getLogger(substr($class, strrpos($class, '\\') + 1));
            register_shutdown_function(array($this, 'shutdown'));
        }

        /**
         * An event triggered before the action is executed
         */
        public function on_before_action() { }

        /**
         * An event triggered after the action is executed
         */
        public function on_after_action() { }

        /**
         * Instantiates a helper class by name
         * @param string $name The name of the helper class
         */
        public function create_helper($name) {
            $name = strtolower($name);
            $class = 'NeuralToys\\YoctoMVC\\Helpers\\' . ucfirst($name) . '_Helper';
            $this->$name = new $class($this->config);
        }

        /**
         * Locates and loads the view and passes all the needed information to it
         * @param array|null $data The collection of variables to pass to the view
         * @param string|null $template The template to use
         * @param string|null $layout The layout to use
         */
        protected final function view($data = null, $template = null, $layout = '') {
            $callers = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
            // special case: if partial_view get previous caller
            $callerIDX = $callers[1]['function'] == 'partial_view' ? 1 : 0;

            if ($template == null) {
                $action = strtolower($callers[$callerIDX + 1]['function']);
                list($controller, /*$type*/) = split_class_name($callers[$callerIDX + 1]['class']);
                $template = $controller . DS . $action;
            }

            $plugin = null;
            $file = $callers[$callerIDX]['file'];
            $plugin_root = $this->config->get('path', 'plugins');
            if (startsWith($file, $plugin_root)) {
                $len = strlen($plugin_root);
                $plugin = substr($file, $len + 1, strpos($file, DS, $len + 1) - $len - 1);
            }

            //create a new view and pass it our template
            $view = new View_Handler($this->config, $this->language, $template, $layout, $plugin);

            // instantiate all needed helpers for the view
            foreach ($this->helpers as $helper) $view->create_helper($helper, $this->$helper);

            //assign article data to view
            if ($data != null) $view->assign($data);
            $view->render();
        }

        /**
         * Loads a view without layout
         * @param array $data The collection of variables to pass to the view
         * @param string $template The template to use
         */
        protected final function partial_view($data = null, $template = null) {
            $this->view($data, $template, null);
        }

        /**
         * Returns a JSON encoded response
         * @param array $data The data to encode
         */
        protected final function json($data) {
            header('Content-Type: application/json');
            echo json_encode($data);
        }

        /**
         * Returns a JSON encoded error message
         * @param string $message The message to return
         */
        protected final function json_error($message) {
            $this->json(array('ok' => false, 'msg' => $message));
        }

        /**
         * Returns a JSON encoded success message
         * @param string $message The message to return
         */
        protected final function json_success($message) {
            $this->json(array('ok' => true, 'msg' => $message));
        }

        /**
         * Redirects the current request to a different location
         * @param string $url The URL to redirect to
         * @param bool $stop Whether the execution should stop immediately
         */
        protected final function redirect($url, $stop = false) {
            if (is_int($url)) Router::http_error($url);
            else header('Location: '.$url);

            if ($stop) exit;
        }

        /**
         * Checks if the used HTTP method is acceptable for this action
         */
        protected final function validate_method() {
            $allowed_methods = func_get_args();
            array_walk($allowed_methods, function(&$val) { $val = strtolower($val); });
            if (!in_array(strtolower($_SERVER['REQUEST_METHOD']), $allowed_methods)) {
                Router::http_error(404);
                exit;
            }
        }

        /**
         * Handles fatal errors to be passed to the logger
         */
        public function shutdown()
        {
            $e = error_get_last();
            if(!is_null($e)) {
                $this->logger->fatal($e['message']);
            }
        }
    }
}