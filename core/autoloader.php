<?php
namespace NeuralToys\YoctoMVC\Core {

    final class AutoLoader {
        private $config;

        public function __construct(Configuration $config) {
            $this->config = $config;
        }

        public function load($className) {
            list($name, $type) = split_class_name($className);
            $folder = '';
            //select the folder where class should be located based on suffix
            switch ($type)
            {
                case '':
                    $folder = 'core';
                    break;

                case 'model':
                    $folder = 'models';
                    break;

                case 'controller':
                    $folder = 'controllers';
                    break;

                case 'helper':
                    $folder = 'helpers';
                    break;

                case 'tool':
                    $folder = 'tools';
                    break;

                case 'library':
                    $folder = 'libraries';
                    break;

                case 'driver':
                    $folder = 'libraries' . DS . 'drivers';
                    break;
            }

            //compose file name
            $file = $this->config->get('path', 'root') . DS . $folder . DS . $name . '.php';
            //fetch file
            if (file_exists($file)) {
                include_once($file);
                return;
            }
            $plugins = $this->config->get('core', 'plugins');
            foreach ($plugins as $plugin) {
                $file = $this->config->get('path', 'plugins') . DS . $plugin . DS . $folder . DS . $name . '.php';
                if (!file_exists($file)) continue;
                include_once($file);
                return;
            }
            die("File '$name' containing class '$className' not found in '$folder'.");
        }
    }
}
