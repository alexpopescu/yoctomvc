<?php
namespace NeuralToys\YoctoMVC\Core {
    /**
     * The Router parses the current URL, extracts all information and then includes the needed files and executes the correct action
     */
    final class Router {
        /**
         * @var Configuration $config The configuration object loaded for the current request
         */
        private $config;
        /**
         * @var string $request The complete query string for the current request
         */
        private $request = null;
        /**
         * @var string $url The url to visit after the current request is complete
         */
        private $url = null;
        /**
         * @var Controller $controller The controller loaded for the current request
         */
        private $controller = null;
        /**
         * @var string $action The action to execute for the current request
         */
        private $action = null;
        /**
         * @var string $plugin The plugin that owns the current request
         */
        private $plugin = null;
        /**
         * @var object $paramObj The object of parameters for the current request
         */
        private $paramObj = null;

        /**
         * Instantiates a router object used to resolve URLs and execute actions
         * @param Configuration $config
         */
        public function __construct(Configuration $config) {
            $this->config = $config;

            // define auto loader
            $autoloader = new AutoLoader($config);
            spl_autoload_register(array($autoloader, 'load'));

            $this->request = $_SERVER['QUERY_STRING'];
            if (!empty($_GET['url'])) $this->url = $_GET['url'];

            // first check if it is a plugin CSS or JS request
            if ($this->check_includes()) return;

            // parse the request to extract controller, action, plugin and parameters
            $this->parse_route();
            $this->parse_vars();

            // load extra files
            $this->load_plugin_extras();

            // then execute the correct action
            $this->execute();
        }

        /**
         * Resolves included resource files such as CSS and JS
         * @return bool True if the include file (css or js) was handled, False if not found or unable to handle
         */
        private function check_includes() {
            if (!preg_match('/(.+)\/(.+)\.(css|js)$/i', $this->url, $matches)) return false;

            $folder = $matches[1];
            $name = explode('.', $matches[2]);
            $extension = $matches[3];
            header("Content-Type: text/".($extension == 'css' ? 'css' : 'javascript'));
            header('Cache-Control: public, must-revalidate, max-age=0');
            header('Pragma: no-cache');

            // check if this file belongs to a plugin
            $this->plugin = array_shift($name);
            $plugin_root = $this->config->get('path', 'plugins');
            if (sizeof($name) > 0) {
                $folder = $plugin_root . DS . $this->plugin . DS . $folder . DS;
                $file = $folder . implode('.', $name) . '.' . $extension;

                // check if it's an existing file
                if (file_exists($file)) {
                    header("Content-Length: " . filesize($file));
                    readfile($file);
                    return true;
                }
            }

            // if a whole folder is targeted then merge the content of all files in it
            $folder = $plugin_root . DS . $this->plugin . DS . $folder . DS;
            $files = glob($folder.'*.'.$extension);
            $size = 0;
            foreach ($files as $file) $size += filesize($file);
            if ($size == 0) {
                self::http_error(404);
                return false;
            }

            header("Content-Length: " . $size);
            foreach ($files as $file) readfile($file);
            return true;
        }

        /**
         * Parse the url and extract the controller, action and if a plugin is targeted or not
         */
        private function parse_route() {
            $this->paramObj = new Parameters();
            if ($this->url) {
                $url = explode('/', $this->url);
                $plugins = $this->config->get('core', 'plugins');
                if (in_array($url[0], $plugins)) $this->plugin = array_shift($url);

                $route = $this->config->get('core', 'route');
                for ($i = 0; $i < count($url) && $i < count($route); $i++) {
                    if ($route[$i] == 'controller') $this->controller = $url[$i];
                    else if ($route[$i] == 'action') $this->action = $url[$i];
                    else $this->paramObj->{$route[$i]} = $url[$i];
                }
            }
            if (empty($this->controller)) $this->controller = $this->config->get('core', 'defaults', 'controller');
            if (empty($this->action)) $this->action = $this->config->get('core', 'defaults', 'action');
            if (empty($this->paramObj->language)) $this->paramObj->language = $this->config->get('core', 'defaults', 'language');
        }

        /**
         * Parse the variables available in the request
         */
        private function parse_vars() {
            // parse GET variables
            foreach ($_GET as $key => $value) {
                if ($key == 'url') continue;
                $this->paramObj->$key = $value;
            }

            // add post variables only if this is a post
            if (strtolower($_SERVER['REQUEST_METHOD']) == 'post')
                foreach ($_POST as $key => $value)
                    $this->paramObj->$key = $value;
        }

        /**
         * Load plugin extras. For each plugin a main file to setup the plugin and an exceptions file are accepted
         */
        private function load_plugin_extras() {
            $plugins = $this->config->get('core', 'plugins');
            $plugins_path = $this->config->get('path', 'plugins');
            foreach ($plugins as $plugin) {
                $file = $plugins_path . DS . $plugin . DS . $plugin . '.php';
                if (file_exists($file)) include_once($file);
                $file = $plugins_path . DS . $plugin . DS . 'exceptions.php';
                if (file_exists($file)) include_once($file);
            }
        }

        /**
         * Execute the current action
         */
        private function execute() {
            //compute the path to the file
            if (!empty($this->plugin)) {
                $target = $this->config->get('path', 'plugins') . DS . $this->plugin . DS . 'controllers' . DS . $this->controller . '.php';
                $namespace = 'NeuralToys\\YoctoMVC\\Plugins\\' . ucfirst($this->plugin) . '\\Controllers\\';
            } else {
                $target = $this->config->get('path', 'root') . DS . 'controllers' . DS . $this->controller . '.php';
                $namespace = 'NeuralToys\\YoctoMVC\\Controllers\\';
            }

            // if target does not exist, try again
            if (!file_exists($target)) {
                self::http_error(404);
                return;
            }

            //get target
            include_once($target);

            //modify page to fit naming convention
            $class_name = str_replace(' ', '_', ucwords(str_replace('-', ' ', $this->controller))) . '_Controller';
            if (empty($this->plugin)) {
                $namespaces = $this->config->get('core', 'namespaces', 'controllers');
                $i = 0;
                while (!class_exists($namespace . $class_name) && $i < sizeof($namespaces)) {
                    if (class_exists($namespaces[$i] . $class_name)) {
                        $namespace = $namespaces[$i];
                        break;
                    }
                    $i++;
                }
            }
            $class = $namespace . $class_name;

            //instantiate the appropriate class
            if (!class_exists($class)) {
                self::http_error(404);
                return;
            }

            $controller = $this->controller;
            $this->controller = new $class($this->config, $this->paramObj->language);
            $this->controller->url->initialize($controller, $this->action, $this->paramObj, $this->plugin, $this->paramObj->language);
            $this->action = str_replace('-', '_', $this->action);
            if (!method_exists($this->controller, $this->action)) {
                self::http_error(404);
                return;
            }

            // call the before action event
            $this->controller->on_before_action();
            // once we have the controller instantiated, execute the action
            $this->controller->{$this->action}($this->paramObj);
            // call the after action event
            $this->controller->on_after_action();
        }

        /**
         * Sets the current response headers to an HTTP error
         * @param int $num The error code
         */
        public static function http_error($num) {
            http_response_code($num);
        }
    }
}
