<?php
namespace NeuralToys\YoctoMVC\Core {

    use stdClass;

    class Parameters extends StdClass {
        private $data = array();
        private $errors;
        private $fields;

        public function __construct() {
            $this->errors = array();
            $this->fields = array();
        }

        public function __set($name, $value) {
            $this->data[$name] = $value;
        }

        public function __get($name) {
            if (array_key_exists($name, $this->data))
                return $this->data[$name];
            return null;
        }

        public function __isset($property) {
            return array_key_exists($property, $this->data) && isset($this->data[$property]);
        }

        public function add_error($message, $property, $field = null) {
            if (empty($field)) $field = $property;
            $this->errors[] = $message;
            $this->fields[] = $field;
        }

        public function to_array() {
            return $this->data;
        }

        private final function validate_type($property, $message, $field, $checker) {
            if (array_key_exists($property, $this->data) && !empty($this->data[$property]) && (empty($checker) || $checker($this->data[$property]))) return true;
            $this->add_error($message, $property, $field);
            return false;
        }

        public final function validate_exists($property, $message = null, $field = null) {
            return $this->validate_type($property, $message, $field, null);
        }

        public final function validate_int($property, $message = null, $field = null) {
            return $this->validate_type($property, $message, $field, 'is_int');
        }

        public final function validate_float($property, $message = null, $field = null) {
            return $this->validate_type($property, $message, $field, 'is_float');
        }

        public final function validate_string($property, $message = null, $field = null) {
            return $this->validate_type($property, $message, $field, 'is_string');
        }

        public final function validate_array($property, $message = null, $field = null) {
            return $this->validate_type($property, $message, $field, 'is_array');
        }

        public final function validate_equals($property, $value, $message = null, $field = null) {
            if (!array_key_exists($property, $this->data) || $this->data[$property] === $value) return true;
            $this->add_error($message, $property, $field);
            return false;
        }

        public final function validate_regex($property, $expression, $message = null, $field = null) {
            if (!array_key_exists($property, $this->data) || preg_match($expression, $this->data[$property])) return true;
            $this->add_error($message, $property, $field);
            return false;
        }
        public final function validate_email($property, $message = null, $field = null) {
            return $this->validate_regex($property, '/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i', $message, $field);
        }

        public final function validate_equals_other($property, $other, $message = null, $field = null) {
            if (!array_key_exists($property, $this->data) || !array_key_exists($other, $this->data)
                || $this->data[$property] === $this->data[$other]) return true;
            $this->add_error($message, $property, $field);
            return false;
        }

        public final function has_errors() {
            return count($this->errors) > 0;
        }

        public final function get_errors() {
            return array(
                'ok' => !$this->has_errors(),
                'msg' => implode("<br />", $this->errors),
                'fields' => $this->fields
            );
        }
    }
}
