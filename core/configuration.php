<?php
namespace NeuralToys\YoctoMVC\Core {
    /**
     * Encapsulates all available configuration, core and plugins
     */
    final class Configuration {
        private $data = array();

        public function __construct($webroot) {
            $rootpath = implode(DS, array_slice(preg_split('/[\/\\\]/', $webroot), 0, -1));

            // define paths
            $this->data['path'] = array(
                'web' => $webroot,
                'root' => $rootpath,
                'core' => $rootpath . DS . 'core',
                'plugins' => $rootpath . DS . 'plugins'
            );

            // load core configuration
            $config_path = $rootpath . DS . 'core' . DS . 'config';
            $config_path = get_first_available_file(array( $config_path . '.ini', $config_path . '.example.ini' ));
            $this->load('core', $config_path);
            $this->data['core']['route'] = explode('/', $this->data['core']['route']);

            // load plugin configurations
            $plugins_available = array_map('basename', glob($this->data['path']['plugins'] . DS . '*' , GLOB_ONLYDIR));
            $plugins_enabled = $this->data['core']['plugins'];
            $this->data['core']['plugins'] = array();
            foreach ($plugins_available as $plugin) {
                $plugin = strtolower($plugin);
                if (empty($plugins_enabled[$plugin])) continue;
                $this->data['core']['plugins'][] = $plugin;
                $config_path = $this->data['path']['plugins'] . DS . $plugin . DS . 'config';
                $config_path = get_first_available_file(array( $config_path . '.ini', $config_path . '.example.ini' ));
                if (file_exists($config_path)) $this->load($plugin, $config_path);
            }
        }

        private function load($name, $file) {
            $name = strtolower($name);
            $new_data = parse_ini_file($file, true);
            if (in_array($name, $this->data))
                $this->data = array_merge_recursive($this->data, $new_data);
            else $this->data[$name] = $new_data;
        }

        public function get() {
            $argc = func_num_args();
            $argv = func_get_args();
            $ret = $this->data;
            $i = 0;
            while ($i < $argc && isset($ret[$argv[$i]])) {
                $ret = $ret[$argv[$i]];
                $i++;
            }
            if ($i == $argc) return $ret;
            return null;
        }
    }
}
