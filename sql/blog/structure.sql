-- Create posts table
CREATE TABLE IF NOT EXISTS `blog_post` (
  `id`             BIGINT UNSIGNED                      NOT NULL AUTO_INCREMENT,
  `user_id`        BIGINT UNSIGNED                      NOT NULL,
  `status`         ENUM('draft', 'published', 'hidden') NOT NULL DEFAULT 'draft',
  `comment_status` ENUM('open', 'closed')               NOT NULL DEFAULT 'open',
  `create_date`    BIGINT                               NOT NULL,
  `update_date`    BIGINT                               NOT NULL,
  `publish_date`   BIGINT                               NULL,
  PRIMARY KEY (`id`),
  KEY `blog_post_user_FK` (`user_id`),
  CONSTRAINT `blog_post_user_FK` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB
  COMMENT = 'Contains blog posts';


-- Create posts translations table
CREATE TABLE IF NOT EXISTS `blog_post_lang` (
  `post_id`          BIGINT UNSIGNED NOT NULL,
  `lang_code`        VARCHAR(10)     NOT NULL,
  `title`            VARCHAR(255)    NOT NULL,
  `title_normalized` VARCHAR(255)    NOT NULL,
  `content`          LONGTEXT        NOT NULL,
  PRIMARY KEY (`post_id`, `lang_code`),
  UNIQUE KEY `title_normalized_UNIQUE` (`title_normalized`),
  KEY `blog_post_lang_post_FK` (`post_id`),
  KEY `blog_post_lang_lang_FK` (`lang_code`),
  CONSTRAINT `blog_post_lang_lang_FK` FOREIGN KEY (`lang_code`) REFERENCES `lang` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blog_post_lang_post_FK` FOREIGN KEY (`post_id`) REFERENCES `blog_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  COMMENT = 'Contains the translated versions of each post';


-- Create post revisions table
CREATE TABLE IF NOT EXISTS `blog_post_revision` (
  `id`             BIGINT UNSIGNED                      NOT NULL AUTO_INCREMENT,
  `post_id`        BIGINT UNSIGNED                      NOT NULL,
  `user_id`        BIGINT UNSIGNED                      NOT NULL,
  `reason`         VARCHAR(255)                         NOT NULL,
  `revision_date`  BIGINT                               NOT NULL,
  PRIMARY KEY (`id`),
  KEY `blog_post_revision_post_FK` (`post_id`),
  KEY `blog_post_revision_user_FK` (`user_id`),
  CONSTRAINT `blog_post_revision_post_FK` FOREIGN KEY (`post_id`) REFERENCES `blog_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `blog_post_revision_user_FK` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB
  COMMENT = 'Contains blog post revisions';


-- Create post revisions translations table
CREATE TABLE IF NOT EXISTS `blog_post_lang_revision` (
  `revision_id`      BIGINT UNSIGNED NOT NULL,
  `lang_code`        VARCHAR(10)     NOT NULL,
  `title_old`        VARCHAR(255)    NOT NULL,
  `title_new`        VARCHAR(255)    NOT NULL,
  `content_old`      LONGTEXT        NOT NULL,
  `content_new`      LONGTEXT        NOT NULL,
  PRIMARY KEY (`revision_id`, `lang_code`),
  KEY `blog_post_lang_revision_revision_FK` (`revision_id`),
  KEY `blog_post_lang_revision_lang_FK` (`lang_code`),
  CONSTRAINT `blog_post_lang_revision_revision_FK` FOREIGN KEY (`revision_id`) REFERENCES `blog_post_revision` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `blog_post_lang_revision_lang_FK` FOREIGN KEY (`lang_code`) REFERENCES `lang` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB
  COMMENT = 'Contains the translated versions of each post revision';


-- Create comments table
CREATE TABLE IF NOT EXISTS `blog_comment` (
  `id`          BIGINT UNSIGNED                       NOT NULL AUTO_INCREMENT,
  `post_id`     BIGINT UNSIGNED                       NOT NULL,
  `user_id`     BIGINT UNSIGNED                       NULL,
  `author`      VARCHAR(255)                          NULL,
  `email`       VARCHAR(255)                          NULL,
  `url`         VARCHAR(255)                          NULL,
  `ip`          VARCHAR(45)                           NOT NULL,
  `content`     TEXT                                  NOT NULL,
  `parent_id`   BIGINT                                NULL,
  `status`      ENUM('draft', 'approved', 'rejected') NOT NULL DEFAULT 'draft',
  `create_date` BIGINT                                NOT NULL,
  PRIMARY KEY (`id`),
  KEY `blog_comment_post_FK` (`post_id`),
  KEY `blog_comment_user_FK` (`user_id`),
  CONSTRAINT `blog_comment_post_FK` FOREIGN KEY (`post_id`) REFERENCES `blog_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `blog_comment_user_FK` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  COMMENT = 'Contains comments to posts';


-- Create tags table
CREATE TABLE IF NOT EXISTS `blog_tag` (
  `id`   BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255)    NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB
  COMMENT = 'Contains tags for posts';


-- Create tags translations table
CREATE TABLE IF NOT EXISTS `blog_tag_lang` (
  `tag_id`    BIGINT UNSIGNED NOT NULL,
  `lang_code` VARCHAR(10)     NOT NULL,
  `name`      VARCHAR(255)    NOT NULL,
  PRIMARY KEY (`tag_id`, `lang_code`),
  KEY `blog_tag_lang_tag_FK` (`tag_id`),
  KEY `blog_tag_lang_lang_FK` (`lang_code`),
  CONSTRAINT `blog_tag_lang_lang_FK` FOREIGN KEY (`lang_code`) REFERENCES `lang` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blog_tag_lang_tag_FK` FOREIGN KEY (`tag_id`) REFERENCES `blog_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  COMMENT = 'Contains translations for tags';


-- Create table to manage connections between posts and tags
CREATE TABLE IF NOT EXISTS `blog_post_tag` (
  `post_id` BIGINT UNSIGNED NOT NULL,
  `tag_id`  BIGINT UNSIGNED NOT NULL,
  `user_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`post_id`, `tag_id`),
  KEY `blog_post_tag_post_FK` (`post_id`),
  KEY `blog_post_tag_tag_FK` (`tag_id`),
  KEY `blog_post_tag_user_FK` (`user_id`),
  CONSTRAINT `blog_post_tag_post_FK` FOREIGN KEY (`post_id`) REFERENCES `blog_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `blog_post_tag_tag_FK` FOREIGN KEY (`tag_id`) REFERENCES `blog_tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `blog_post_tag_user_FK` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  COMMENT = 'Defines connections between posts and tags';
