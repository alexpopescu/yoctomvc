
DELIMITER $$


DROP procedure IF EXISTS `blog_post_get`$$
CREATE PROCEDURE `blog_post_get` (
  IN p_id BIGINT UNSIGNED)
  BEGIN
    -- =============================================
    -- Author:		  Alex Popescu
    -- Create date: 13/08/2013
    -- Description:	Retrieves a blog post by id
    -- =============================================
    SELECT bp.*
    FROM `blog_post` bp
    WHERE bp.`id` = p_id;
  END
$$


DROP procedure IF EXISTS `blog_post_get_by_title`$$
CREATE PROCEDURE `blog_post_get_by_title` (
  IN p_normalized_title VARCHAR(255))
  BEGIN
    -- =============================================
    -- Author:		  Alex Popescu
    -- Create date: 08/08/2013
    -- Description:	Retrieves a blog post by normalized name
    -- =============================================
    SELECT bp.*, bpl.`title`, bpl.`content`,
      COALESCE(CONCAT(au.`first_name`, " ", au.`last_name`), au.`username`) AS `author`
    FROM `blog_post_lang` bpl
      LEFT JOIN `blog_post` bp ON bp.`id` = bpl.`post_id`
      LEFT JOIN `account_user` au ON bp.`user_id` = au.`id`
    WHERE bpl.`title_normalized` = p_normalized_title;
  END
$$


DROP procedure IF EXISTS `blog_post_lang_get_by_post`$$
CREATE PROCEDURE `blog_post_lang_get_by_post` (
  IN p_post_id BIGINT UNSIGNED)
  BEGIN
    -- =============================================
    -- Author:		  Alex Popescu
    -- Create date: 09/08/2013
    -- Description:	Retrieves all translations of a blog post by the post id
    -- =============================================
    SELECT bpl.`lang_code`, l.`original_name` AS `lang_name`, bpl.`title`, bpl.`content`
    FROM `lang` l
    LEFT JOIN `blog_post_lang` bpl ON bpl.`lang_code` = l.`code` AND bpl.`post_id` = p_post_id;
  END
$$


DROP procedure IF EXISTS `blog_comment_get_by_post`$$
CREATE PROCEDURE `blog_comment_get_by_post` (
  IN p_post_id BIGINT UNSIGNED)
  BEGIN
    -- =============================================
    -- Author:		  Alex Popescu
    -- Create date: 08/08/2013
    -- Description:	Retrieves a list of comments for a post
    -- =============================================
    SELECT bc.*,
      COALESCE(CONCAT(au.`first_name`, " ", au.`last_name`), au.`username`, bc.`author`) AS `author`
    FROM `blog_comment` bc
      LEFT JOIN `account_user` au ON bc.`user_id` = au.`id`
    WHERE bc.`post_id` = p_post_id;
  END
$$


DROP procedure IF EXISTS `blog_post_save`$$
CREATE PROCEDURE `blog_post_save` (
  IN p_id BIGINT UNSIGNED,
  IN p_user_id BIGINT UNSIGNED,
  IN p_status VARCHAR(255),
  IN p_comment_status VARCHAR(255))
  BEGIN
    -- =============================================
    -- Author:		  Alex Popescu
    -- Create date: 09/08/2013
    -- Description:	Creates a new post or changes the status of an existing post
    -- =============================================
    IF p_id IS NULL OR p_id = 0 THEN
      INSERT INTO `blog_post` (`user_id`, `status`, `comment_status`, `create_date`, `update_date`, `publish_date`)
        VALUES (p_user_id, p_status, p_comment_status, UNIX_TIMESTAMP(), UNIX_TIMESTAMP(), IF(p_status = 'published', UNIX_TIMESTAMP(), NULL));
      SELECT LAST_INSERT_ID() AS id;
    ELSE
      UPDATE `blog_post` SET
        `publish_date` = IF(p_status = 'published', IF(`status` != 'published', UNIX_TIMESTAMP(), `publish_date`), NULL),
        `status` = p_status,
        `comment_status` = p_comment_status,
        `update_date` = UNIX_TIMESTAMP()
      WHERE `id` = p_id;
      SELECT p_id AS id;
    END IF;
  END
$$


DROP procedure IF EXISTS `blog_post_lang_save`$$
CREATE PROCEDURE `blog_post_lang_save` (
  IN p_post_id BIGINT UNSIGNED,
  IN p_lang_code VARCHAR(10),
  IN p_title VARCHAR(255),
  IN p_title_normalized VARCHAR(255),
  IN p_content VARCHAR(255),
  IN p_user_id BIGINT UNSIGNED,
  IN p_revision_reason VARCHAR(255),
  IN p_revision_id BIGINT UNSIGNED)
  BEGIN
    -- =============================================
    -- Author:		  Alex Popescu
    -- Create date: 09/08/2013
    -- Description:	Creates or updates a blog post translation
    -- =============================================
    START TRANSACTION;

    IF EXISTS(SELECT 1 FROM `blog_post_lang` WHERE `post_id` = p_post_id AND `lang_code` = p_lang_code) THEN
      -- check if an update is needed
      IF EXISTS(SELECT 1 FROM `blog_post_lang` WHERE `post_id` = p_post_id AND `lang_code` = p_lang_code AND (`title` != p_title OR `content` != p_content)) THEN
        -- check if there is a revision, if not create it
        IF (p_revision_id IS NULL OR p_revision_id <= 0) THEN
          INSERT INTO `blog_post_revision` (`post_id`, `user_id`, `reason`, `revision_date`)
            VALUES (p_post_id, p_user_id, p_revision_reason, UNIX_TIMESTAMP());
          SELECT LAST_INSERT_ID() INTO p_revision_id;
        END IF;
        -- create the revision lang entry
        INSERT INTO `blog_post_lang_revision` (`revision_id`, `lang_code`, `title_old`, `title_new`, `content_old`, `content_new`)
          SELECT p_revision_id, p_lang_code, `title`, p_title, `content`, p_content FROM `blog_post_lang`
          WHERE `post_id` = p_post_id AND `lang_code` = p_lang_code;
        -- do the actual update
        UPDATE `blog_post_lang` SET
          `title` = p_title,
          `title_normalized` = p_title_normalized,
          `content` = p_content
        WHERE `post_id` = p_post_id AND `lang_code` = p_lang_code;
      END IF;
    ELSE
      -- insert post translation
      INSERT INTO `blog_post_lang` (`post_id`, `lang_code`, `title`, `title_normalized`, `content`)
        VALUES (p_post_id, p_lang_code, p_title, p_title_normalized, p_content);
    END IF;

    SELECT p_revision_id AS `revision_id`;

    COMMIT;

  END
$$


DROP procedure IF EXISTS `blog_post_delete`$$
CREATE PROCEDURE `blog_post_delete` (
  IN p_id BIGINT UNSIGNED)
  BEGIN
    -- =============================================
    -- Author:		  Alex Popescu
    -- Create date: 26/08/2013
    -- Description:	Deletes an existing post
    -- =============================================
    DECLARE EXIT HANDLER FOR NOT FOUND ROLLBACK;
    DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;

    START TRANSACTION;

    DELETE FROM `blog_post_lang_revision`
      WHERE `revision_id` IN (SELECT `id` FROM `blog_post_revision` WHERE `post_id` = p_id);
    DELETE FROM `blog_post_revision` WHERE `post_id` = p_id;
    DELETE FROM `blog_post_lang` WHERE `post_id` = p_id;
    DELETE FROM `blog_post` WHERE `id` = p_id;

    COMMIT;
  END
$$


DELIMITER ;

