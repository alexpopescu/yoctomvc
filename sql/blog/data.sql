
-- Add needed rights
CALL account_right_save('blog_post_add');
CALL account_right_save('blog_post_delete');
CALL account_right_save('blog_comment_delete');

-- Add relationships between roles and rights
CALL account_role_right_name_save('administrator', 'blog_post_add');
CALL account_role_right_name_save('administrator', 'blog_post_delete');
CALL account_role_right_name_save('administrator', 'blog_comment_delete');
