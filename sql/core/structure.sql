
-- Table that contains available languages
CREATE TABLE IF NOT EXISTS `language` (
  `code`          VARCHAR(10)                NOT NULL,
  `english_name`  VARCHAR(45)                NOT NULL,
  `original_name` VARCHAR(45)                NOT NULL,
  `status`        ENUM('active', 'inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`code`))
  ENGINE = InnoDB
  COMMENT = 'Contains available languages';

-- Table that contains available timezones
CREATE  TABLE IF NOT EXISTS `timezone` (
  `pk_timezone`   SMALLINT UNSIGNED  NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50)        NOT NULL,
  PRIMARY KEY (`pk_timezone`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
  ENGINE = InnoDB
  COMMENT = 'Contains available timezones';

-- Create session table
CREATE TABLE IF NOT EXISTS `session` (
  `pk_session` varchar(32) NOT NULL,
  `last_access` int(10) unsigned DEFAULT NULL,
  `data` text,
  PRIMARY KEY (`pk_session`)
) ENGINE=InnoDB
  COMMENT = 'This table contains the session information';
