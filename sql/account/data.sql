
-- Add needed roles
INSERT IGNORE INTO `account_role` (`name`) VALUES ('administrator');

-- Add needed rights
INSERT IGNORE INTO `account_right` (`name`) VALUES ('manage_users');
INSERT IGNORE INTO `account_right` (`name`) VALUES ('delete_users');
INSERT IGNORE INTO `account_right` (`name`) VALUES ('manage_rights_roles');

-- Add relationships between roles and rights
INSERT IGNORE INTO `account_role_right` (`fk_role`, `fk_right`)
VALUES ((SELECT `pk_role` FROM `account_role` WHERE `name` = 'administrator'), (SELECT `pk_right` FROM `account_right` WHERE `name` = 'manage_users'));

INSERT IGNORE INTO `account_role_right` (`fk_role`, `fk_right`)
VALUES ((SELECT `pk_role` FROM `account_role` WHERE `name` = 'administrator'), (SELECT `pk_right` FROM `account_right` WHERE `name` = 'delete_users'));

INSERT IGNORE INTO `account_role_right` (`fk_role`, `fk_right`)
VALUES ((SELECT `pk_role` FROM `account_role` WHERE `name` = 'administrator'), (SELECT `pk_right` FROM `account_right` WHERE `name` = 'manage_rights_roles'));

