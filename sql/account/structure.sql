
-- Create users table
CREATE TABLE IF NOT EXISTS `account_user` (
  `pk_user`     BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email`    	VARCHAR(255)                                      NOT NULL,
  `first_name`  VARCHAR(50)                                       NULL,
  `last_name`   VARCHAR(50)                                       NULL,
  `password`    VARCHAR(45)                                       NOT NULL,
  `url`         VARCHAR(255)                                      NULL,
  `status`      ENUM('unconfirmed','active','inactive','deleted') NOT NULL DEFAULT 'unconfirmed',
  `fk_timezone` SMALLINT UNSIGNED                                 NULL,
  `create_date` BIGINT                                            NOT NULL,
  `update_date` BIGINT                                            NOT NULL,
  `key`         VARCHAR(32)                                       NULL,
  PRIMARY KEY (`pk_user`),
  UNIQUE INDEX `unq_email` (`email` ASC),
  UNIQUE INDEX `unq_key` (`key` ASC),
  KEY `fk_timezone` (`fk_timezone`),
  CONSTRAINT `fk_timezone` FOREIGN KEY (`fk_timezone`) REFERENCES `timezone` (`pk_timezone`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB
  COMMENT = 'This table contains the user credentials and details';

-- Create rights table
CREATE TABLE IF NOT EXISTS `account_right` (
  `pk_right`   	BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` 		VARCHAR(45)     NOT NULL,
  PRIMARY KEY (`pk_right`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC)
) ENGINE = InnoDB
  COMMENT = 'This table contains all rights that can be assigned to a user';

-- Create roles table
CREATE TABLE IF NOT EXISTS `account_role` (
  `pk_role`   	BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` 		VARCHAR(45)     NOT NULL,
  PRIMARY KEY (`pk_role`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC)
) ENGINE = InnoDB
  COMMENT = 'This table contains all roles that can be assigned to a user';

-- Create the table that maps rights to roles
CREATE TABLE IF NOT EXISTS `account_role_right` (
  `fk_role`  BIGINT UNSIGNED NOT NULL,
  `fk_right` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`fk_role`, `fk_right`))
  ENGINE = InnoDB
  COMMENT = 'This table contains connections between roles and rights';
-- Define foreign keys to rights and roles
ALTER TABLE `account_role_right`
ADD CONSTRAINT `account_role_right_role_FK`
FOREIGN KEY (`fk_role`)
REFERENCES `account_role` (`pk_role`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `account_role_right_right_FK`
FOREIGN KEY (`fk_right`)
REFERENCES `account_right` (`pk_right`)
  ON DELETE CASCADE
  ON UPDATE CASCADE
, ADD INDEX `account_role_right_role_FK` (`fk_role` ASC)
, ADD INDEX `account_role_right_right_FK` (`fk_right` ASC);

-- Create the table that contains roles granted to users
CREATE TABLE IF NOT EXISTS `account_user_role` (
  `fk_user`    BIGINT UNSIGNED NOT NULL,
  `fk_role`    BIGINT UNSIGNED NOT NULL,
  `granted_by` BIGINT UNSIGNED NOT NULL,
  `grant_date` BIGINT          NOT NULL,
  PRIMARY KEY (`fk_user`, `fk_role`))
  ENGINE = InnoDB
  COMMENT = 'Defines roles assigned to each user';
-- Define foreign keys to users and roles
ALTER TABLE `account_user_role`
ADD CONSTRAINT `account_user_role_user_FK`
FOREIGN KEY (`fk_user`)
REFERENCES `account_user` (`pk_user`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `account_user_role_role_FK`
FOREIGN KEY (`fk_role`)
REFERENCES `account_role` (`pk_role`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `account_user_role_granted_by_FK`
FOREIGN KEY (`granted_by`)
REFERENCES `account_user` (`pk_user`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `account_user_role_user_FK` (`fk_user` ASC)
, ADD INDEX `account_user_role_role_FK` (`fk_role` ASC)
, ADD INDEX `account_user_role_granted_by_FK` (`granted_by` ASC);

-- Create the table that contains rights granted directly to users
CREATE TABLE IF NOT EXISTS `account_user_right` (
  `fk_user`    BIGINT UNSIGNED NOT NULL,
  `fk_right`   BIGINT UNSIGNED NOT NULL,
  `granted_by` BIGINT UNSIGNED NOT NULL,
  `grant_date` BIGINT          NOT NULL,
  PRIMARY KEY (`fk_user`, `fk_right`))
  ENGINE = InnoDB
  COMMENT = 'Defines rights assigned directly to each user';
-- Define foreign keys to users and rights
ALTER TABLE `account_user_right`
ADD CONSTRAINT `account_user_right_user_FK`
FOREIGN KEY (`fk_user`)
REFERENCES `account_user` (`pk_user`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `account_user_right_right_FK`
FOREIGN KEY (`fk_right`)
REFERENCES `account_right` (`pk_right`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `account_user_right_granted_by_FK`
FOREIGN KEY (`granted_by`)
REFERENCES `account_user` (`pk_user`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `account_user_right_user_FK` (`fk_user` ASC)
, ADD INDEX `account_user_right_right_FK` (`fk_right` ASC)
, ADD INDEX `account_user_right_granted_by_FK` (`granted_by` ASC);
