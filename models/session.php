<?php
namespace NeuralToys\YoctoMVC\Models {
    use NeuralToys\YoctoMVC\Core\Configuration;
    use NeuralToys\YoctoMVC\Core\Model;

    /**
     * The Session Model deals with session database management
     */
    class Session_Model extends Model {

        public function __construct(Configuration $config) {
            parent::__construct($config);
            parent::connect();
        }

        public function load_session($pk_session) {
            $ret = parent::single('SELECT `data` FROM `session` WHERE `pk_session` = :pk_session', array( 'pk_session' => $pk_session ));
            if (!$ret) return null;
            return json_decode($ret['data']);
        }

        public function save_session($pk_session, $data) {
            $access = time();

            return parent::execute('REPLACE INTO `session` (`pk_session`, `last_access`, `data`) VALUES (:pk_session, :access, :data)', array(
                'pk_session' => $pk_session,
                'access' => $access,
                'data' => json_encode($data)
            ));
        }

        public function delete_session($pk_session) {
            return parent::delete('sessions', array( 'pk_session' => $pk_session ));
        }

        public function clean_expired_sessions($lifetime) {
            $last_access = time() - $lifetime;
            return parent::execute('DELETE FROM `session` WHERE `last_access` < :last_access', array( 'last_access' => $last_access ));
        }
    }
}