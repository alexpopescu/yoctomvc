<?php
namespace NeuralToys\YoctoMVC\Models {
    use NeuralToys\YoctoMVC\Core\Model;

    /**
     * The Language Model deals with timezone data
     */
    class Language_Model extends Model {

        public function all() {
            return parent::query('SELECT `code`, `original_name` FROM `language` WHERE `status` = \'active\'');
        }

    }
}