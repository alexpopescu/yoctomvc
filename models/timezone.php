<?php
namespace NeuralToys\YoctoMVC\Models {
    use NeuralToys\YoctoMVC\Core\Model;

    /**
     * The Timezone Model deals with timezone data
     */
    class Timezone_Model extends Model {

        public function all() {
            return parent::query('SELECT `pk_timezone`, `name` FROM `timezone`');
        }

    }
}