<?php
use NeuralToys\YoctoMVC\Core\Configuration;
use NeuralToys\YoctoMVC\Core\Router;

define('VERSION', '1.0.1');
define('DS', DIRECTORY_SEPARATOR);
define('DEBUG', true);
$webroot = preg_replace($_SERVER['PHP_SELF'].'$/i', '', $_SERVER['SCRIPT_FILENAME']);
$webroot = rtrim($webroot, DS);

$core_folder = $webroot . DS . '..' . DS . 'core' . DS;

// Custom Exceptions
require_once($core_folder . 'exceptions.php');

// Configuration manager
require_once($core_folder . 'configuration.php');

// Language manager
require_once($core_folder . 'language.php');

// Load the auto loader
require_once($core_folder . 'autoloader.php');

// Load the functions TODO maybe there is a better way for this
require_once($core_folder . 'functions.php');

// Load the base controller class
require_once($core_folder . 'controller.php');

// View Handler
require_once($core_folder . 'view.php');

// Parameters class used to pass data to actions
require_once($core_folder . 'parameters.php');

// The external module manager
require_once($webroot . DS . '..' . DS . 'externals' . DS . 'externals.php');

// Router class that manages all
require_once($core_folder . 'router.php');
new Router(new Configuration($webroot));

unset($core_folder);
unset($webroot);
