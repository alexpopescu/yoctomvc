// pub-sub mechanism
// inspired by https://github.com/mennovanslooten/jquery.instawidget.js
(function ($, window, document, undefined) {

    var _callbacks = {},
        _subscribers = {};

    var setup = function (name, callback) {
        var selector = '[data-' + name + ']';
        var flag = name + '-done';

        _callbacks[selector] = function () {
            var $element = $(this),
                data = $element.data(name);

            if ($element.data(flag)) return;
            callback($element, data);
            $element.data(flag, true);
        };
    };

    $.fn.rescan = function () {
        var $this = this;
        if ($this.length > 0) {
            $.each(_callbacks, function (selector, setup) {
                $this.filter(selector).each(setup);
                $this.find(selector).each(setup);
            });
        }
        return this;
    };

    // Redefine APPEND
    $.fn.append = (function (oldAppend) {
        return function () {
            var $children = this.children(),
                result = oldAppend.apply(this, arguments);
            if ($children.length > 0) $children.last().nextAll().rescan();
            else this.children().rescan();
            return result;
        }
    }($.fn.append));

    // Redefine PREPEND
    $.fn.prepend = (function (oldPrepend) {
        return function () {
            var $children = this.children(),
                result = oldPrepend.apply(this, arguments);
            if ($children.length > 0) $children.first().prevAll().rescan();
            else this.children().rescan();
            return result;
        }
    }($.fn.prepend));

    // Redefine HTML
    $.fn.html = (function (oldHtml) {
        return function () {
            var result = oldHtml.apply(this, arguments);
            this.children().rescan();
            return result;
        }
    }($.fn.html));

    // Redefine BEFORE
    $.fn.before = (function (oldBefore) {
        return function () {
            var $prev = this.prev(),
                result = oldBefore.apply(this, arguments);
            if ($prev.length > 0) this.prevUntil($prev).rescan();
            else this.prevAll().rescan();
            return result;
        }
    }($.fn.before));

    // Redefine AFTER
    $.fn.after = (function (oldAfter) {
        return function () {
            var $next = this.next(),
                result = oldAfter.apply(this, arguments);
            if ($next.length > 0) this.nextUntil($next).rescan();
            else this.nextAll().rescan();
            return result;
        }
    }($.fn.after));

    // setup publish functionality
    setup('publish', function ($element, pubs) {
        var pubPairs = pubs.split(';');
        $.each(pubPairs, function (index, pub) {
            var pubPair = pub.split(':'),
                pubEvent = pubPair[0],
                pubSubject = pubPair[1],
                parameters = /([^\(]+)(\(([^\)]+)\))?/.exec(pubSubject);
            pubSubject = parameters[1];
            parameters = parameters[3];

            if (parameters) {
                parameters = parameters.split(',');
                for (var p = 0; p < parameters.length; p++) {
                    parameters[p] = parameters[p].split('=');
                }
            }

            $element.on(pubEvent, function (e) {
                var callbacks = _subscribers[pubSubject];
                if (!callbacks) return;

                $.each(callbacks, function (callbackIndex, callback) {
                    var args = {};
                    if (parameters) {
                        $.each(parameters, function (pIndex, p) {
                            args[p[0]] = p[1].startsWith('#') ? $(p[1]).val() : (/^[-0-9\.]+$/.exec(p[1]) ? parseFloat(p[1]) : p[1]);
                        });
                    }
                    callback($element, args);
                });
            });
        });
    });

    // setup subscribe functionality
    setup('subscribe', function ($element, subs) {
        var subPairs = subs.split(';');
        $.each(subPairs, function (index, sub) {
            var subPair = sub.split(':'),
                subEvent = subPair[1],
                subSubject = subPair[0];

            if (!(subSubject in _subscribers)) {
                _subscribers[subSubject] = [];
            }

            _subscribers[subSubject].push(function ($publisher, args) {
                $element.trigger(subEvent, args, $publisher);
            });
        });
    });

    $(function () {
        $(document).rescan();
    });

})(jQuery, window, document);