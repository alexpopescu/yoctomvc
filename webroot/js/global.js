$(function() {
    var $body = $(document.body);

    // handle form submission
    $body.on('click', 'button[data-submit]', function() {
        var $this = $(this),
            $form = $($this.data('submit')),
            $success = $($form.data('success')),
            $error = $($form.data('error'));

        $form.find('.has-error').removeClass('has-error');

        $.ajax({
            url: $form.attr('action'),
            method: $form.attr('method'),
            data: $form.serializeArray(),
            success: function(data) {
                if (!data.ok) {
                    $error.html(data.msg).show();
                    for (var i = 0; i < data.fields.length; i++)
                        $('#' + data.fields[i]).closest('.form-group').addClass('has-error');
                    $success.hide();
                    return;
                }
                $success.html(data.msg).show();
                $error.hide();
                var $modal = $this.closest('div.modal');
                if ($modal.length > 0) {
                    $modal.modal('hide');
                    $form.trigger('saved');
                } else $form.trigger('refresh');
            }
        });
    });


    var get_data_interval = null;
    var get_data = function($form, url, id) {

        if ($form.find('select[data-url]:not([data-loaded="1"])').length > 0) {
            if (get_data_interval == null) {
                get_data_interval = setInterval(function() { get_data($form, url, id); }, 500);
            }
            return;
        }
        clearInterval(get_data_interval);

        $.ajax({
            url: url,
            data: {
                id: id
            },
            success: function (data) {
                $form.find('[name]').each(function () {
                    var $this = $(this),
                        name = $this.attr('name'),
                        value = name in data ? data[name] : '',
                        type = $this.attr('type'),
                        composed = name.match(/^([^\[]+)\[([^\]]+)]$/);

                    // special case handling of radio buttons
                    if (type == 'radio') {
                        $this.prop('checked', $this.attr('value') == value);
                        return;
                    }
                    // special case handling of checkboxes
                    if (type == 'checkbox') {
                        $this.prop('checked', value);
                        return;
                    }

                    if (composed && composed[1] in data){
                        var subdata = data[composed[1]];

                        // TODO: finalize special case handling of wysihtml5 editors
                        if (typeof $this.data('wysihtml5') !== 'undefined') {
                            $this.data('wysihtml5').editor.setValue(subdata && composed[2] in subdata ? subdata[composed[2]] : '');
                        } else {
                            $this.val(subdata && composed[2] in subdata ? subdata[composed[2]] : '');
                        }
                        return;
                    }

                    // TODO: finalize special case handling of wysihtml5 editors
                    if (typeof $this.data('wysihtml5') !== 'undefined') {
                        $this.data('wysihtml5').editor.setValue(data);
                        return;
                    }

                    if ($this.data('role') == 'tags') {
                        $this.tagsinput('removeAll');
                        if (value.constructor === Array) {
                            for (var i = 0; i < value.length; i++) {
                                $this.tagsinput('add', value[i]);
                            }
                        } else {
                            $this.tagsinput('add', value);
                        }
                    } else $this.val(value);
                });
            }
        });
    };


    $('form[data-source]').on('refresh', function() {
        var $this = $(this);
        get_data($this, $this.data('source'));
    }).each(function() { $(this).trigger('refresh'); });

    // add + edit record
    $('table[data-editor]').on('click', 'tbody span.glyphicon-edit,thead span.glyphicon-plus', function () {
        var $this = $(this),
            id = $this.closest('tr').attr('id'),
            $table = $this.closest('table.dataTable'),
            $modal = $($table.data('editor')),
            $form = $modal.find('form');

        if (id) {
            $('#' + $form.attr('id') + '-id').val(id);
            get_data($form, $modal.data('get-url'), id);
        } else {
            $form.find('[name]').each(function () {
                var $this = $(this);
                if ($this.data('role') == 'tags') $this.tagsinput('removeAll');
                else if ($this.prop('tagName').toLowerCase() == 'select') $this.val($this.children('option:first').attr('value'));
                else if ($.inArray($this.attr('type'), ['radio', 'checkbox']) == -1) $this.val('');
            });
        }

        $modal.find('.modal-title').text($modal.attr('data-' + (id ? 'edit' : 'add') + '-title'));
        $modal.modal('show');
    }).each(function() {
        // set up the connection between table and form
        var $table = $(this),
            editor = $table.data('editor');
        if (!editor) return;
        var $modal = $($table.data('editor')),
            $form = $modal.find('form');
        if ($form.length == 0) return;

        // handle form events
        $form.on('saved', function() {
            $table.DataTable().ajax.reload();
        }).on('submit', function(event) {
            event.preventDefault();
            $modal.find('button[data-submit]').trigger('click');
        });

        // handle modal events
        $modal.on('show.bs.modal', function() {
            $($table.data('success') + ',' + $table.data('error')).hide();
        }).on('shown.bs.modal', function() {
            $form.find(':input:visible:enabled:first').focus();
        });
    });


    // url action icons
    // this will handle any icon with a url action attached to it
    $body.on('click', 'span.glyphicon[data-url]', function () {
        var $this = $(this),
            $table = $this.closest('table'),
            confirm = $this.data('confirm'),
            func = function(finish) {
                var options = {
                    url: $this.data('url'),
                    success: function (data) {
                        var $success = $($table.data('success')),
                            $error = $($table.data('error'));

                        if (!data.ok) {
                            $error.html(data.msg).show();
                            $success.hide();
                            return;
                        }
                        $success.html(data.msg).show();
                        $error.hide();
                        $table.DataTable().ajax.reload();
                        if (finish != undefined) finish();
                    }
                };
                var id = $this.closest('tr').attr('id');
                if (id) options['data'] = { id: id };
                $.ajax(options);
            };
        if (confirm) window.confirm(func, $this.data('confirm'));
        else func();
    });


    // auto-loading drop down menus
    $('li.dropdown[data-url]').on('refresh', function(event, args) {
        var $this = $(this),
            url = $this.data('url'),
            linkProp = $this.data('link'),
            labelProp = $this.data('label'),
            $subMenu = $this.find('ul.dropdown-menu'),
            $title = $this.find('span[role="title"]');
        $.ajax({
            url: url,
            data: args,
            success: function(data) {
                if (!data.items || data.items.length == 0) {
                    $this.hide();
                    return;
                }

                var html = data.items.map(function (e, i) {
                    return '<li><a tabindex="-1" href="' + e[linkProp] + '">' + e[labelProp] + '</a></li>';
                });
                $title.html(data.title);
                $subMenu.html(html);
                $this.show();
            }
        });
    }).each(function() {
        $(this).trigger('refresh');
    });

    // auto-loading drop-downs
    $('select[data-url]').on('refresh', function(event, args) {
        var $this = $(this),
            url = $this.data('url'),
            value = $this.data('value'),
            name = $this.data('name'),
            selected = $this.data('selected'),
            nullable = $this.data('nullable'),
            groupName = $this.data('group-name'),
            groupContent = $this.data('group-content');

        $.ajax({
            url: url,
            data: args,
            success: function(data) {
                var first = nullable ? '<option value=""></option>' : '';
                if (data == null) {
                    $this.html(first);
                    return;
                }
                var options = function (e, i) {
                    var val = value && value in e ? e[value] : e,
                        label = name && name in e ? e[name] : val,
                        sel = selected && selected in e ? e[selected] : i == 0;
                    return '<option value="' + val + '"' + (sel ? ' selected' : '') + '>' + label + '</option>';
                };

                var html = '';
                if (groupName && groupContent) {
                    html = data.map(function (e, i) {
                        return '<optgroup label="' + e[groupName] + '">' + e[groupContent].map(options) + '</optgroup>';
                    });
                } else {
                    html = data.map(options);
                }

                $this.html(first + html);
                $this.attr('data-loaded', '1');
            }
        });
    }).each(function() {
        $(this).trigger('refresh');
    });


    // auto initialize tag input based on attributes
    $('[data-role="tags"]').each(function() {
        var $input = $(this);

        var engine = new Bloodhound({
            remote: $input.data('source'),
            identify: function(obj) { return obj[$input.data('identity')]; },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            datumTokenizer: Bloodhound.tokenizers.whitespace('name')
        });
        engine.initialize();

        $input.data('options', {
            tagClass: 'label label-default',
            itemText: $input.data('display'),
            itemValue: $input.data('identity'),
            freeInput: false,
            typeaheadjs: {
                displayKey: $input.data('display'),
                name: $input.attr('name'),
                source: engine.ttAdapter()
            }
        });
        $input.tagsinput($input.data('options'));
    });


    // bootstrap confirmation dialog
    window.confirm = function(callback, message, title) {
        var $confirm = $("#modal-confirm");
        if (typeof(title) != 'undefined') {
            $confirm.find('.modal-header').html(title);
        }
        $confirm.find('.modal-body p').html(message);
        $confirm.find('button[data-function="yes"]').off('click').click(function () {
            callback(function() {
                $confirm.modal("hide");
            });
        });
        $confirm.find('button[data-function="no"]').off('click').click(function () {
            $confirm.modal("hide");
        });
        $confirm.modal('show');
    };


    // define automatic loader for ajax calls
    var $loader = $('[role="loader"]');
    if ($loader.length) {
        $(document).ajaxSend(function (event, request, settings) {
            $loader.modal('show');
        });

        $(document).ajaxComplete(function (event, request, settings) {
            $loader.modal('hide');
        });
    }

});