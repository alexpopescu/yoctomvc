<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php $this->html->charset(); ?>
    <title><?php if ($this->title) echo $this->title; ?></title>
    <link rel="shortcut icon" href="/favicon.ico" />

    <!-- CSS -->
    <?php $this->html->css('/css/bootstrap-3.3.5'.(DEBUG ? '' : '.min').'.css?'); ?>
    <?php $this->html->css('/css/dataTables.bootstrap-1.10.8'.(DEBUG ? '' : '.min').'.css?'); ?>
    <?php $this->html->css('/css/bootstrap-tagsinput'.(DEBUG ? '' : '.min').'.css?'); ?>
    <?php $this->html->css('/css/main.css?'.VERSION); ?>

    <!-- Fav and touch icons -->
    <!--<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">-->

    <?php $this->section('header', true); ?>
</head>

<body role="document">
<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><?php echo $this->config->get('core', 'application'); ?></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Home</a></li>
                <?php echo $this->menu('left'); ?>
                <li><a href="/about">About</a></li>
                <li><a href="/contact">Contact</a></li>

                <?php if ($this->session->get('account', 'pk_user') > 0) { ?>
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->language->get('account_management_title', 'account'); ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a tabindex="-1" href="<?php echo $this->url->action('user', 'listing', null, 'account'); ?>"><?php echo $this->language->get('users_title', 'account'); ?></a></li>
                            <li><a tabindex="-1" href="<?php echo $this->url->action('auth', 'roles', null, 'account'); ?>"><?php echo $this->language->get('roles_title', 'account'); ?></a></li>
                            <li><a tabindex="-1" href="<?php echo $this->url->action('auth', 'rights', null, 'account'); ?>"><?php echo $this->language->get('rights_title', 'account'); ?></a></li>
                        </ul>
                    </li>
                <?php } ?>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php echo $this->menu('right'); ?>
                <li class="dropdown" data-url="<?php echo $this->url->action('global', 'languages', array( 'req' => $this->url->current(array('language' => '[[-LANG-]]')))); ?>" data-link="url" data-label="name">
                    <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><span role="title"></span><b class="caret"></b></a>
                    <ul class="dropdown-menu"></ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container" role="main">
    <div class="row">
        <!--<?php if ($this->session->get('account', 'id') > 0) { ?>
            Hello <?php echo $this->session->get('account', 'name'); ?>,
            <a href="<?php echo $this->url->action('user', 'profile', null, 'account'); ?>">
                <?php echo $this->language->get('my_profile', 'account'); ?></a>
            <a href="<?php echo $this->url->action('flow', 'logout', null, 'account'); ?>" data-ajax="post">
                <?php echo $this->language->get('sign_out', 'account'); ?></a>
        <?php } else { ?>
            <a href="<?php echo $this->url->action('flow', 'login', null, 'account'); ?>">
                <?php echo $this->language->get('sign_in_title', 'account'); ?><b class="caret"></b></a>
        <?php } ?>
        <br /><hr />-->

        <?php if ($this->title) { ?>
            <h1 class="page-header"><?php echo $this->title; ?></h1>
        <?php } ?>

        <div data-alert="success" class="alert alert-success" style="display: none;"></div>
        <div data-alert="error" class="alert alert-danger" style="display: none;"></div>

        <?php echo $this->content; ?>
    </div>

    <footer>
        <p>&copy; YoctoMVC <?php echo date('Y'); ?></p>
    </footer>
</div>

<div id="modal-confirm" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><?php echo $this->language->get('confirmation'); ?></h4>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button data-function="yes" type="button" class="btn btn-primary"><?php echo $this->language->get('yes_option'); ?></button>
                <button data-function="no" type="button" class="btn btn-default"><?php echo $this->language->get('no_option'); ?></button>
            </div>
        </div>
    </div>
</div>

<div role="loader" class="modal fade" style="font-size: 2em; color: #AAA;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4"><span class="glyphicon glyphicon-refresh spinning"></span></div>
                        <div class="col-md-8"><?php echo $this->language->get('loading'); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- JavaScript -->
<?php $this->html->script('/js/libs/jquery-2.1.4'.(DEBUG ? '' : '.min').'.js'); ?>
<?php $this->html->script('/js/libs/bootstrap-3.3.5'.(DEBUG ? '' : '.min').'.js'); ?>
<?php $this->html->script('/js/libs/jquery.dataTables-1.10.8'.(DEBUG ? '' : '.min').'.js'); ?>
<?php $this->html->script('/js/libs/dataTables.bootstrap-1.10.8'.(DEBUG ? '' : '.min').'.js'); ?>
<?php $this->html->script('/js/libs/typeahead.bundle-0.11.1'.(DEBUG ? '' : '.min').'.js'); ?>
<?php $this->html->script('/js/libs/bootstrap-tagsinput'.(DEBUG ? '' : '.min').'.js'); ?>

<?php $this->html->script('/js/extensions.js?'.VERSION); ?>
<?php $this->html->script('/js/pubsub.js?'.VERSION); ?>
<?php $this->html->script('/js/init.bootstrap.js?'.VERSION); ?>
<?php $this->html->script('/js/global.js?'.VERSION); ?>
<?php foreach($this->config->get('core', 'plugins') as $plugin) $this->html->script('/js/'.$plugin.'.main.js?'.VERSION); ?>
<?php $this->section('script', true); ?>

</body>
</html>
