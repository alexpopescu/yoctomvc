<?php
namespace NeuralToys\YoctoMVC\Tools {
    use DateTime;
    use DateTimeZone;

    class Date_Tool {
        const TIMEZONE = 'UTC';
        /**
         * Holds variables assigned to template
         */

        public static function now() {
            $dt = new DateTime('now', new DateTimeZone(self::TIMEZONE));
            return $dt->getTimestamp();
        }

        public static function timestamp_to_date($timestamp, $timezone = null) {
            $dt = new DateTime('@'.$timestamp);
            if ($timezone == null) return $dt;
            if (is_string($timezone)) $timezone = new DateTimeZone($timezone);
            $dt->setTimezone($timezone);
            return $dt;
        }
    }
}