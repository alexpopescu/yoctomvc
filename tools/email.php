<?php
namespace NeuralToys\YoctoMVC\Tools {

    use InvalidArgumentException;
    use UnexpectedValueException;

    class Email_Tool {
        const REGEX = '/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i';
        /**
         * Holds variables assigned to template
         */
        private $data = array();
        public function __get($name) {
            if (array_key_exists($name, $this->data))
                return $this->data[$name];

            return null;
        }

        private $config = null;
        private $template = array();
        //private $from = array();
        //private $to = array();
        private $cc = array();
        private $bcc = array();
        private $subject = null;

        public static function validate($address) {
            return preg_match(self::REGEX, $address);
        }

        function __construct($config) {
            $this->config = $config;
            $from_name = $this->config->get('core', 'email', 'sender_name');
            $from_email = $this->config->get('core', 'email', 'sender_email');
            $this->from($from_email, $from_name);
        }

        public function template($template, $mime = 'text/plain', $charset = 'utf-8', $plugin = null) {
            if (empty($plugin))
                $file = $this->config->get('path', 'root') . DS . 'views' . DS . strtolower($template) . '.php';
            else
                $file = $this->config->get('path', 'plugins') . DS . $plugin . DS . 'views' . DS . strtolower($template) . '.php';
            if (file_exists($file)) $this->template[] = array('template' => $file, 'mime' => $mime, 'charset' => $charset);
        }

        private function set_address($type, $address, $name = null) {
            if (empty($address) || (is_array($address) && count($address) == 0))
                throw new InvalidArgumentException('No addresses specified for "'.$type.'"');
            if (!self::validate($address))
                throw new InvalidArgumentException('Invalid email address specified for "'.$type.'": '.$address);
            array_push($this->$type, array('address' => $address, 'name' => $name));
        }

        private function get_address($type) {
            $ret = '';
            foreach ($this->$type as $address) {
                $ret .= (empty($ret) ? '' : ', ');
                if (empty($address['name']))
                    $ret .= $address['address'];
                else $ret .= $address['name'] . ' <' . $address['address'] .'>';
            }
            return $ret;
        }

        public function from($address, $name = null) { $this->set_address('from', $address, $name); }
        public function to($address, $name = null) { $this->set_address('to', $address, $name); }
        public function cc($address, $name = null) { $this->set_address('cc', $address, $name); }
        public function bcc($address, $name = null) { $this->set_address('bcc', $address, $name); }

        public function subject($subject) {
            $this->subject = $subject;
        }

        public function variable($name, $value) {
            if (empty($name))
                throw new InvalidArgumentException('Argument cannot be empty: "name"');
            $this->data[$name] = $value;
        }

        public function send() {
            // check for meta injection in variables
            $find = "/(content-type|bcc:|cc:)/i";
            foreach ($this->data as $key => $value)
                if (preg_match($find, $value))
                    throw new UnexpectedValueException('Meta injection detected in parameter: '.$key);

            $parts = count($this->template);
            // parse templates
            for ($i = 0; $i < $parts; $i++) {
                ob_start();
                include($this->template[$i]['template']);
                $this->template[$i]['content'] = ob_get_clean();
            }

            $headers = array();
            $sep = "\r\n";
            if ($parts == 1) {
                $headers[] = 'Content-type: '.$this->template[0]['mime'].'; charset='.$this->template[0]['charset'];
                $body = $this->template[0]['content'];
            } else {
                // generate some boundaries
                $boundary1 = rand(0,9) . "-" . rand(10000000000,9999999999) . "-" . rand(10000000000,9999999999) . "=:" . rand(10000,99999);
                //$boundary2 = rand(0,9) . "-" . rand(10000000000,9999999999) . "-" . rand(10000000000,9999999999) . "=:" . rand(10000,99999);

                $headers[] = 'MIME-Version: 1.0';
                $headers[] = 'Content-Type: multipart/alternative; boundary="'.$boundary1.'"';

                // define body
                $body = 'This is a multi-part message in MIME format.'.$sep.$sep;
                foreach ($this->template as $tpl)
                    $body .= '--'.$boundary1.$sep.'Content-Type: '.$tpl['mime'].'; charset="'.$tpl['charset'].'"'
                        .$sep.'Content-Transfer-Encoding: quoted-printable'.$sep.$sep.$tpl['content'].$sep;
                $body .= $sep.'--'.$boundary1.'--'.$sep;
            }
            $headers[] = 'From: ' . $this->get_address('from');
            if (!empty($this->cc) && count($this->cc) > 0)
                $headers[] = 'Cc: ' . $this->get_address('cc');
            if (!empty($this->bcc) && count($this->bcc) > 0)
                $headers[] = 'Bcc: ' . $this->get_address('bcc');
            //$headers[] = "Reply-To: Recipient Name <receiver@domain3.com>";
            //$headers[] = 'Subject: ' . $this->subject;
            $headers[] = 'X-Mailer: PHP/' . phpversion();

            $to = $this->get_address('to');

            mail($to, $this->subject, $body, implode($sep, $headers));
        }
    }
}