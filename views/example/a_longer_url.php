
You can put dashes in URLs:<br/>
<a href="<?php echo $this->url->action('example', 'a-longer-url'); ?>"><?php echo $this->url->action('example', 'a-longer-url'); ?></a>
<br /><br />


And the language:<br/>
<a href="<?php echo $this->url->action('example', 'a-longer-url', array('language' => 'fr')); ?>">
    <?php echo $this->url->action('example', 'a-longer-url', array('language' => 'fr')); ?>
</a>
<br /><br />
