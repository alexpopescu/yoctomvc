
It "yocto"-works!
<br/><br/>
Some generated URLs:<br/>
<a href="<?php

echo $this->url->action('example', 'index');

?>">Simple action</a>
<br/>
<a href="<?php

echo $this->url->action('example', 'index', array(
    'id' => 34,
    'other' => 'us'
));

?>">Action with special parameter</a>
<br/>
<a href="<?php

echo $this->url->action('example', 'index', array(
    'id' => 34,
    'test' => 'yes'
));

?>">Action with special and custom parameters</a>
<br/>
<a href="<?php

echo $this->url->action('example', 'index', array(
    'id' => 34,
    'test' => 'yes'
), 'account');

?>">Action with special and custom parameters inside plugin</a>
<br/>
<a href="<?php

echo $this->url->action('example', 'index', array(
    'id' => 34,
    'test' => 'yes'
), 'account', false);

?>">Action with relative URL</a>
