<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <?php $this->html->charset(); ?>
    <title><?php if ($this->data && isset($this->data->title)) echo $this->data->title; ?></title>
    <link rel="shortcut icon" href="/favicon.ico" />

    <!-- CSS -->
    <!--<?php $this->html->css('/css/main.css?'.VERSION); ?>-->

    <!-- Fav and touch icons -->
    <!--<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">-->

    <?php $this->section('header', true); ?>
</head>

<body>

<?php if ($this->session->get('account', 'id') > 0) { ?>
    Hello <?php echo $this->session->get('account', 'name'); ?>,
    <a href="<?php echo $this->url->action('user', 'profile', null, 'account'); ?>">
        <?php echo $this->language->get('my_profile', 'account'); ?></a>
    <a href="<?php echo $this->url->action('flow', 'logout', null, 'account'); ?>" data-ajax="post">
        <?php echo $this->language->get('sign_out', 'account'); ?></a>
<?php } else { ?>
    <a href="<?php echo $this->url->action('flow', 'login', null, 'account'); ?>">
        <?php echo $this->language->get('sign_in_title', 'account'); ?><b class="caret"></b></a>
<?php } ?>
<br /><hr />

    <?php echo $this->content; ?>

    <hr />

    <footer>
        <p>&copy; Company <?php echo date('Y'); ?></p>
    </footer>

    <!-- JavaScript -->
    <?php $this->html->script('/js/global.js?'.VERSION); ?>

</body>
</html>
