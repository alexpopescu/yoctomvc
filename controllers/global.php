<?php
namespace NeuralToys\YoctoMVC\Controllers {
    use Exception;
    use NeuralToys\YoctoMVC\Core\Controller;
    use NeuralToys\YoctoMVC\Core\Parameters;
    use NeuralToys\YoctoMVC\Core\Router;
    use NeuralToys\YoctoMVC\Models\Language_Model;
    use NeuralToys\YoctoMVC\Models\Timezone_Model;

    /**
     * This file handles some generic calls
     */
    class Global_Controller extends Controller
    {
        public function messages(Parameters $params)
        {
            $this->view(array_merge($params->to_array(), array('messages' => $this->session->get_messages())));
        }

        public function timezones()
        {
            try {
                $model = new Timezone_Model($this->config);
                $timezones = $model->all();
                $assoc = array();
                foreach ($timezones as $timezone) {
                    $continent = substr($timezone['name'], 0, strpos($timezone['name'], '/'));
                    if (array_key_exists($continent, $assoc)) {
                        $assoc[$continent][] = $timezone;
                    } else {
                        $assoc[$continent] = array($timezone);
                    }
                }
                $ret = array();
                foreach ($assoc as $key => $value) {
                    $ret[] = array(
                        'name' => $key,
                        'zones' => $value
                    );
                }
                $this->json($ret);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }

        public function languages(Parameters $params)
        {
            try {
                $model = new Language_Model($this->config);
                $languages = $model->all();
                $current = $this->language->code();
                $title = '';
                $ret = array();
                $requested_url = $params->req;
                if ($requested_url[0] != '/' && !startsWith($requested_url, 'http')) $requested_url = '/' . $requested_url;

                foreach ($languages as $lang) {
                    if ($lang['code'] == $current) {
                        $title = $lang['original_name'];
                        continue;
                    }
                    $ret[] = array(
                        'code' => $lang['code'],
                        'name' => $lang['original_name'],
                        'current' => $lang['code'] == $current,
                        'url' => str_replace('[[-LANG-]]', $lang['code'], $requested_url)
                    );
                }
                $this->json(array('title' => $title, 'items' => $ret));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                $this->json_error($this->language->get('msg_error'));
            }
        }
    }
}
