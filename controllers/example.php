<?php
namespace NeuralToys\YoctoMVC\Controllers {

    use NeuralToys\YoctoMVC\Core\Controller;
    use NeuralToys\YoctoMVC\Core\Parameters;

    /**
     * This file handles the homepage
     */
    class Example_Controller extends Controller
    {
        public function index(Parameters $params) {
            $this->view($params);
        }

        public function a_longer_url(Parameters $params) {
            $this->view($params);
        }
    }
}
