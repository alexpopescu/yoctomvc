<?php
namespace NeuralToys\YoctoMVC\Helpers {

    class Bootstrap_Helper extends Base_Helper {

        public function css() {
            $args = func_get_args();
            foreach ($args as $arg)
                echo sprintf('<link rel="stylesheet" type="text/css" href="%s" />
', $arg);
        }

        public function charset($charset = null) {
            global $_CONFIG;
            if (empty($charset)) $charset = strtolower($_CONFIG['CORE']['encoding']);
            echo sprintf('<meta http-equiv="Content-Type" content="text/html; charset=%s" />',
                (!empty($charset) ? $charset : 'utf-8'));
        }

        public function script() {
            $args = func_get_args();
            foreach ($args as $arg)
                echo sprintf('<script type="text/javascript" src="%s"></script>
', $arg);
        }

        public function form_editable($form, $name, $value, $parameters = null) {
            //$label = null, $editable = true, $placeholder = null, $options = null) {
            $type = empty($parameters['type']) ? 'text' : $parameters['type'];
            $ret = '<div class="form-group">';
            if (!empty($parameters['label']))
                $ret .= '<label class="col-3 control-label" for="'.$form.'-'.$name.'">'.$parameters['label'].'</label>';
            $ret .= '<div class="col-9'.($parameters['editable'] ? ' editable' : '')
                .'" data-var-name="'.$name.'"><p class="form-control-static">'.$value.'</p>';
            if ($parameters['editable']) {
                $ret .= $this->field_markup($form.'-'.$name, $value, $parameters);
                switch($type) {
                    case 'select':
                        $ret .= '<select id="'.$form.'-'.$name.'" class="form-control">';
                        if (!empty($parameters['options'])) {
                            foreach ($parameters['options'] AS $key => $val) {
                                if (is_array($val)) {
                                    $ret .= '<optgroup label="'.$key.'">';
                                    foreach ($val AS $subkey => $subval)
                                        $ret .= '<option value="'.$subkey.'"'.($subkey == $value || $subval == $value ? ' selected' : '').'>'.$subval.'</option>';
                                } else
                                    $ret .= '<option value="'.$key.'"'.($key == $value || $val == $value ? ' selected' : '').'>'.$val.'</option>';
                            }
                        }
                        $ret .= '</select>';
                        break;
                    default:
                        $ret .= '<input type="'.$type.'" id="'.$form.'-'.$name.'" class="form-control" value="'.$value.'" />';
                }
            }
            $ret .= '</div></div>';
            echo $ret;
        }

        public function form_field($name, $value, $parameters = null) {
            $type = empty($parameters['type']) ? 'text' : $parameters['type'];
            $ret = '<div class="form-group">';
            if (!empty($parameters['label']) && $type != 'checkbox')
                $ret .= '<label class="col-3 control-label" for="'.$name.'">'.$parameters['label'].'</label>';
            $ret .= '<div class="col-9'.(empty($parameters['hint']) ? '' : ' input-group').'">';
            $ret .= $this->field_markup($name, $value, $parameters);
            if (!empty($parameters['hint']))
                $ret .= '<span class="input-group-btn"><button class="btn btn-default" type="button" data-placement="right"'
                    .' data-trigger="click" title="'.$parameters['hint'].'">?</button></span>';
            $ret .= '</div></div>';
            echo $ret;
        }

        private function field_markup($name, $value, $parameters = null) {
            $type = empty($parameters['type']) ? 'text' : $parameters['type'];
            $ret = '';
            switch($type) {
                case 'html':
                    $ret .= '<textarea id="'.$name.'" class="form-control" rows="10"'
                        .(empty($parameters['placeholder']) ? '' : ' placeholder="'.$parameters['placeholder'].'"').'>'.$value.'</textarea>';
                    break;
                case 'select':
                    $ret .= '<select id="'.$name.'" class="form-control">';
                    if (!empty($parameters['options'])) {
                        foreach ($parameters['options'] AS $key => $val) {
                            if (is_array($val)) {
                                $ret .= '<optgroup label="'.$key.'">';
                                foreach ($val AS $subkey => $subval)
                                    $ret .= '<option value="'.$subkey.'"'.($subkey == $value || $subval == $value ? ' selected="selected"' : '').'>'.$subval.'</option>';
                            } else
                                $ret .= '<option value="'.$key.'"'.($key == $value || $val == $value ? ' selected="selected"' : '').'>'.$val.'</option>';
                        }
                    }
                    $ret .= '</select>';
                    break;
                case 'checkbox':
                    $ret .= '<label><input type="checkbox" id="'.$name.'" '.($value ? 'checked="checked"' : '').'/>';
                    if (!empty($parameters['label'])) $ret .= $parameters['label'];
                    $ret .= '</label>';
                    break;
                default:
                    $ret .= '<input type="'.$type.'" id="'.$name.'" class="form-control" value="'.$value.'"'
                        .(empty($parameters['placeholder']) ? '' : ' placeholder="'.$parameters['placeholder'].'"').' />';
            }
            return $ret;
        }
    }
}