<?php
namespace NeuralToys\YoctoMVC\Helpers {
    use BadMethodCallException;
    use Exception;
    use NeuralToys\YoctoMVC\Models\Session_Model;

    final class Session_Helper extends Base_Helper
    {
        private static $methods = array();

        /** @var Session_Model */
        private $model;

        const MESSAGE_TYPE_ERROR = 'error';
        const MESSAGE_TYPE_SUCCESS = 'success';

        public function __call($name, $arguments)
        {
            if (empty(self::$methods[$name]))
                throw new BadMethodCallException('Method not found: ' . $name);
            array_unshift($arguments, $this);
            return call_user_func_array(self::$methods[$name], $arguments);
        }

        public static function add_method($name, $function)
        {
            self::$methods[$name] = $function;
        }

        public function get()
        {
            if (empty($_SESSION)) return null;
            $argc = func_num_args();
            $argv = func_get_args();
            $ret = $_SESSION;
            $i = 0;
            while ($i < $argc && isset($ret[$argv[$i]])) {
                $ret = $ret[$argv[$i]];
                $i++;
            }
            if ($i == $argc) return $ret;
            return null;
        }

        public function set()
        {
            $argc = func_num_args() - 1;
            if ($argc < 1) throw new Exception('invalid number of arguments');
            $argv = func_get_args();
            $value = $argv[$argc];
            $ret = &$_SESSION;
            $i = 0;
            while ($i < $argc) {
                if (!isset($ret[$argv[$i]]))
                    $ret[$argv[$i]] = array();
                $ret = &$ret[$argv[$i]];
                $i++;
            }
            $ret = $value;
        }

        public function clear($name = null)
        {
            if (empty($name)) session_unset();
            else unset($_SESSION[$name]);
        }

        public function add_message($type, $text)
        {
            $messages = self::get_messages();
            $messages[] = array('type' => $type, 'text' => $text);
            self::set('messages', $messages);
        }

        public function get_messages()
        {
            $messages = self::get('messages');
            if (!is_array($messages)) $messages = array();
            return $messages;
        }

        public function clear_messages()
        {
            self::clear('messages');
        }

        public function start($name = null)
        {
            session_set_save_handler(
                array($this, '_open'),
                array($this, '_close'),
                array($this, '_read'),
                array($this, '_write'),
                array($this, '_destroy'),
                array($this, '_gc')
            );
            if ($name != null) session_name($name);
            session_start();
        }

        public function _open()
        {
            $this->model = new Session_Model($this->config);
        }

        public function _close()
        {
            $this->model = null;
        }

        public function _read($pk_session)
        {
            return $this->model->load_session($pk_session);
        }

        public function _write($pk_session, $data)
        {
            return $this->model->save_session($pk_session, $data);
        }

        public function _destroy($pk_session)
        {
            return $this->model->delete_session($pk_session);
        }

        public function _gc($lifetime)
        {
            return $this->model->clean_expired_sessions($lifetime);
        }
    }
}
