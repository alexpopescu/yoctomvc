<?php
namespace NeuralToys\YoctoMVC\Helpers {

    class Url_Helper extends Base_Helper {
        private $controllerVal;
        private $actionVal;
        private $parametersVal;
        private $pluginVal;
        private $languageVal;

        public function initialize($controller, $action, $parameters, $plugin, $language) {
            $this->controllerVal = $controller;
            $this->actionVal = $action;
            $this->parametersVal = $parameters;
            $this->pluginVal = $plugin;
            $this->languageVal = $language;
        }

        public function current($replacements = null, $absolute = true) {
            $contr = $this->controllerVal;
            $act = $this->actionVal;
            $param = $this->parametersVal;
            $plg = $this->pluginVal;
            if ($replacements != null && is_array($replacements)) {
                foreach ($replacements AS $key => $value) {
                    if ($key == 'controller') $contr = $value;
                    else if ($key == 'action') $act = $value;
                    else $param->$key = $value;
                }
            }
            return $this->action($contr, $act, $param->to_array(), $plg, $absolute);
        }

        public function action($controller = null, $action = null, $parameters = null, $plugin = null, $absolute = true) {
            $route = $this->config->get('core', 'route');
            $defaults = $this->config->get('core', 'defaults');
            $defaults['language'] = $this->languageVal;

            $parts = array();
            foreach ($route as $r) {
                if ($r == 'controller') $parts[$r] = $controller ?: $defaults['controller'];
                else if ($r == 'action') $parts[$r] = $action ?: $defaults['action'];
                else if ($parameters != null && array_key_exists($r, $parameters)) $parts[$r] = $parameters[$r];
            }

            $url = '';
            $found = false;

            // first check the route
            for ($i = count($route) - 1; $i >= 0; $i--) {
                $is_param = $route[$i] != 'controller' && $route[$i] != 'action';
                $has_value = !empty($parts[$route[$i]]);
                $is_default = isset($defaults[$route[$i]]) && isset($parts[$route[$i]]) && $parts[$route[$i]] == $defaults[$route[$i]];

                if ($is_param && !$has_value && isset($defaults[$route[$i]])) $parts[$route[$i]] = $defaults[$route[$i]];

                if ($found || ($has_value && !$is_default) || $route[$i] == 'language') {
                    $url = $parts[$route[$i]].(!empty($url) ? '/'.$url : '');
                    $found = true;
                }
            }

            // add the rest of parameters
            $query = '';
            if ($parameters != null && is_array($parameters) && count($parameters) > 0) {
                foreach ($parameters as $key => $value) {
                    if (empty($value) || !trim($value) || in_array($key, $route)) continue;
                    $query .= (!empty($query) ? '&' : '').$key.'='.urlencode($value);
                }
                if (!empty($query)) $url .= '?'.$query;
            }

            // add the plugin if any
            if (!empty($plugin)) $url = $plugin.'/'.$url;

            if ($absolute) $url = $this->make_absolute($url);
            return $url;
        }

        public function make_absolute($url) {
            if (preg_match('/^[a-z]+:\/\//i', $url)) return $url;
            if (substr($url,0,1) != '/') $url = '/'.$url;
            $port = $_SERVER['SERVER_PORT'] == '80' ? '' : ':'.$_SERVER['SERVER_PORT'];
            $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http';
            return $protocol.'://'.$_SERVER['SERVER_NAME'].$port.$url;
        }
    }
}