<?php
namespace NeuralToys\YoctoMVC\Helpers {

    use NeuralToys\YoctoMVC\Core\Configuration;

    class Base_Helper {
        protected $config;

        public function __construct(Configuration $config) {
            $this->config = $config;
        }
    }
}