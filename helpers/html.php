<?php
namespace NeuralToys\YoctoMVC\Helpers {

    class Html_Helper extends Base_Helper
    {
        public function css()
        {
            $args = func_get_args();
            foreach ($args as $arg) {
                $this->open_tag('link', array(
                    'rel' => 'stylesheet',
                    'type' => 'text/css',
                    'href' => $arg
                ), true);
            }
        }

        public function script()
        {
            $args = func_get_args();
            foreach ($args as $arg) {
                $this->open_tag('script', array(
                    'type' => 'text/javascript',
                    'src' => $arg
                ));
                $this->close_tag('script');
            }
        }

        public function charset($charset = null)
        {
            global $_CONFIG;
            if (empty($charset)) $charset = strtolower($_CONFIG['CORE']['encoding']);
            if (empty($charset)) $charset = 'utf-8';
            $this->open_tag('meta', array(
                'http-equiv' => 'Content-Type',
                'content' => 'text/html; charset=' . $charset
            ), true);
        }

        public function open_tag($name, $attributes, $empty = false)
        {
            echo '<' . $name;
            foreach ($attributes as $key => $value)
                echo ' ' . $key . '="' . $value . '"';
            echo ($empty ? ' />' : '>') . '
';
        }

        public function close_tag($name)
        {
            echo '</' . $name . '>
';
        }
    }
}