![YoctoMVC] (http://www.alexandrupopescu.com/yoctomvc/logo_short_alpha.png)
# YoctoMVC #

A minimalist [MVC] framework made with [PHP]

### The Idea ###

This project started when I wanted to find a very simple MVC framework to build my site upon and I wasn't really happy with the available solutions. Most of the frameworks out there are way too complicated or contain way too much code for my taste. So I decided to create a new one that will have as main purpose to contain the minimum quantity of code necessary to make it work and only the features I really need. Another intended feature of YoctoMVC is the ability to easily add plugins just by copying them and configuring (optionally) and each plugin will be structured as an MVC application itself.

### The Name ###
[Yocto] is a prefix in the metric system and (till today) it's the smallest official SI-prefix. It was chosen to represent minimalism in this case.

### Quick Start ###

1. Drop all files in a local folder
2. Edit the configuration file located at **/core/configuration.php** if needed
3. Set the root of the site to be the **/webroot/** folder
4. Enjoy

### Structure Description ###

- **/controllers/**: the controller classes should be located here
- **/core/**: this folder contains the core of the framework
- **/helpers/**: the helper classes have to be placed here
- **/models/**: the model classes have to be placed here
- **/plugins/**: the folder that contains all plugins, one per subfolder
- **/sql/**: an auxiliary folder containing necessary MySQL scripts
- **/views/**: the views have to be placed in this folder, layouts in the folder directly and the ones belonging to a controller should be located in a subfolder named as the controller
- **/webroot/**: the root of the website, will contain content files, such as CSS, images or JavaScript files

### Features ###

##### URL Rewrite #####
YoctoMVC includes the necessary files to make use of URL rewriting on both [Apache] (.htaccess) and [IIS] (web.config) web servers. You can delete the one you don't need after installing but you don't need to as they will be outside of the web root and thus not publicly readable.

##### Automatic Selection of Views #####
In any action from any controller, the view output can be triggered by using the **view** method of the base controller class.
```php
$this->view($data, $template, $layout);
```
If the template is not specified then the base controller will automatically look for a template with the same name as the caller action method in a folder with the same name as the caller controller.
For example having the following code
```php
class Test_Controller extends Controller {

    public function index(StdClass $params) {
        // ...
		$this->view();
    }
}
```
will attempt to load as a template the file **/views/test/index.php**.

##### Layouts #####
Layouts are views used as a container for other views. Layout files are located in the **/views/** folder and can have any name. The layout to use can be specified for each action when calling the view method (see above).
If the layout to be used is not specified, then the one defined in **/core/configuration.php** will be used as default. The configuration below:
```ini
[defaults]
...
layout = "master"
```
will attempt to use the file **/views/master.php**

##### Sections #####
Sections are portions of the layout to be defined by the view. They can be defined as:
```html
<!-- SectionStart: header -->
<link rel="stylesheet" type="text/css" href="/css/blog.css" />
<!-- SectionEnd -->
```
and then displayed in the layout using:
```php
    <?php $this->section('header', true); ?>
```
where the second parameter specifies if the section is optional or not. If not optional and the section does not exist then the engine will throw an exception.

### List of known Plugins ###

- [Account][p1] (incomplete): an account management plugin including administration of users, rights and roles
- [Blog][p2] (incomplete): a blog component

### Thanks ###
- The font in the logo is [Captain Podd][font]. The site of the author does not exist anymore but... thanks, whoever you are.

### License ###

YoctoMVC is protected by the [MIT] license

> Copyright &copy; 2013 Alex Popescu, Neural Toys
>
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
> associated documentation files (the "Software"), to deal in the Software without restriction, 
> including without limitation the rights to use, copy, modify, merge, publish, distribute, 
> sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
> furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all copies or 
> substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
> BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
> NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
> DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[mvc]: http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller
[php]: http://www.php.net/
[yocto]: http://en.wikipedia.org/wiki/Yocto
[apache]: http://httpd.apache.org/
[iis]: http://www.iis.net/
[mit]: http://opensource.org/licenses/MIT

[p1]: https://github.com/neuraltoys/yoctomvc-account
[p2]: https://github.com/neuraltoys/yoctomvc-blog

[font]: http://www.dafont.com/captain-podd.font
