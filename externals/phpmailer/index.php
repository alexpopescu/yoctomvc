<?php

$dir = dirname(__FILE__);
require($dir . '/class.phpmailer.php');
require($dir . '/class.pop3.php');
require($dir . '/class.smtp.php');
unset($dir);

Externals::add_static_method('getPhpMailer', function(\NeuralToys\YoctoMVC\Core\Configuration $config) {
    $mail = new PHPMailer(true);
    //$mail->SMTPDebug = 3;                               // Enable verbose debug output

    $mail->isSMTP();
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    $mail->Host = $config->get('core', 'mail', 'smtp_host');
    $mail->SMTPAuth = true;
    $mail->Username = $config->get('core', 'mail', 'smtp_username');
    $mail->Password = $config->get('core', 'mail', 'smtp_password');
    $mail->SMTPSecure = $config->get('core', 'mail', 'smtp_secure');
    $mail->Port = $config->get('core', 'mail', 'smtp_port');
    $mail->CharSet = 'UTF-8';

    $sender_email = $config->get('core', 'mail', 'sender_email');
    $sender_name = $config->get('core', 'mail', 'sender_name');
    $mail->From = $sender_email;
    $mail->FromName = $sender_name;
    $mail->addReplyTo($sender_email, $sender_name);

    return $mail;
});
