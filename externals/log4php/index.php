<?php

$dir = dirname(__FILE__);
require($dir . DS . 'Logger.php');
Logger::configure(get_first_available_file(array($dir . DS . 'config.xml', $dir . DS . 'config.example.xml')));
unset($dir);

Externals::add_static_method('getLogger', function($name) {
    return Logger::getLogger($name);
});
