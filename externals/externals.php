<?php

final class Externals {
    private static $methods = array();

    public static function __callStatic($name, $arguments) {
        if (empty(self::$methods[$name]))
            throw new BadMethodCallException('Method not found: '.$name);
        return call_user_func_array(self::$methods[$name], $arguments);
    }

    public static function add_static_method($name, $function) {
        self::$methods[$name] = $function;
    }

    public static function load($name) {
        $file = dirname(__FILE__) . DS . $name . DS . 'index.php';
        if (!$name || !file_exists($file)) throw new InvalidArgumentException('External module not found: ' . $file);
        require_once($file);
    }

}